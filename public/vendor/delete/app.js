function deleteRegister(id_register,page_register){
    $.confirm({
        title: 'Excluir o registro!',
        content: 'Deseja excluir o registro?',
        buttons: {
            confirm: function () {
                $.ajax({
                type: "POST",
                url: "/dashboard/admin/"+page_register+"/del",
                data: {
                    id: id_register
                },
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data,status){
                    if(data == 1 && status == 'success'){

                        $("#status_"+id_register).removeClass("badge-success");
                        $("#status_"+id_register).addClass("badge-danger");
                        $("#status_"+id_register).html("Inativo");
                        $("#button_"+id_register).addClass("disabled");


                            Swal.fire({
                                icon:  'success',
                                title: 'Registro excluído.',
                            })
                        }else{
                            Swal.fire({
                                icon:  'error',
                                title: 'Erro ao excluir o registro.',
                            })
                    }
                }
                });
            },
            cancel: function () {
                Swal.fire({
                        icon:  'info',
                        title: 'Cancelado, a exclusão do registro.',
                    })
            }
        }
    });
}
