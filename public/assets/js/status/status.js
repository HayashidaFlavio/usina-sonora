function updateStatus(id_register,page_register){

    status_register = $("#hidden_status_"+id_register).val();
    if($("#hidden_status_"+id_register).val() == 1){
        status_register = 0;
    }else{
        status_register = 1;
    }
    $.confirm({
        title: 'Alterar o status!',
        content: 'Deseja alterar o status?',
        buttons: {
            confirm: function () {
                $.ajax({
                type: "POST",
                url: "/dashboard/admin/"+page_register+"/status",
                data: {
                    id: id_register,
                    stats: status_register
                },
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data,status){
                    if(data == 1 && status == 'success'){
                            if(status_register == 1)
                            {
                                $("#status_"+id_register).removeClass("badge-danger");
                                $("#status_"+id_register).addClass("badge-success");
                                $("#status_"+id_register).html("Ativo");
                                $("#hidden_status_"+id_register).val('1').trigger('change');
                                $("#button_view_"+id_register).removeClass("disabled");
                                $("#button_del_"+id_register).removeClass("disabled");
                                $("#button_edit_"+id_register).removeClass("disabled");

                            }else{
                                $("#status_"+id_register).removeClass("badge-success");
                                $("#status_"+id_register).addClass("badge-danger");
                                $("#status_"+id_register).html("Inativo");
                                $("#hidden_status_"+id_register).val('0').trigger('change');
                                $("#button_view_"+id_register).addClass("disabled");
                                $("#button_del_"+id_register).addClass("disabled");
                                $("#button_edit_"+id_register).addClass("disabled");

                            }

                            Swal.fire({
                                icon:  'success',
                                title: 'Status alterado.',
                            })

                        }else{

                            Swal.fire({
                                icon:  'error',
                                title: 'Erro ao alterar o status.',
                            })
                    }
                }
                });
            },
            cancel: function () {
                Swal.fire({
                        icon:  'info',
                        title: 'Cancelado, a aleração do registro.',
                    })
            }
        }
    });
}
