$("#passwordGenerate").on('click',function() {
    //var formData =  $("form").serialize();
    $("#passwordGenerate").addClass("disabled");
    $.ajax({
        url: '/dashboard/admin/investor/passwordGenerate',
        type: 'POST',
        data: {
                id: $("#id").val(),
                name: $("#name").val(),
                email: $("#email").val(),
                password: $("#password").val(),
        },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
        },
        success: function( data,status ) {
            if(data == 1 && status == 'success'){
            $("#passwordGenerate").removeClass("disabled");
            Swal.fire({
                icon:  'success',
                title: 'Senha criada com sucesso.',
            })
            }else{
                Swal.fire({
                    icon:  'error',
                    title: 'Erro não foi possível enviar a senha.',
                })
            }
        },
        error: function (request, status, error) {
            alert(request.responseText);
        }
    });
});
