(function($) {

	"use strict";

	$('nav .dropdown').hover(function(){
		var $this = $(this);
		$this.addClass('show');
		$this.find('> a').attr('aria-expanded', true);
		$this.find('.dropdown-menu').addClass('show');
	}, function(){
		var $this = $(this);
			$this.removeClass('show');
			$this.find('> a').attr('aria-expanded', false);
			$this.find('.dropdown-menu').removeClass('show');
	});

})($);
$("document").ready(function($){

    var nav = $('#MainMenu');
    var logo1 = $('#logo1');
    var logo2 = $('#logo2');

    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            nav.addClass("sticky");
            logo1.fadeOut();
            logo1.addClass('sr-only');
            logo2.fadeIn();
            logo2.removeClass('sr-only');

        }  else {
            nav.removeClass("sticky");
            logo1.fadeIn();
            logo1.removeClass('sr-only');
            logo2.fadeOut();
            logo2.addClass('sr-only');
        }
    });

});
