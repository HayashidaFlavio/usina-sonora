$("#fileUpload").on('click',function() {
    $("#fileUpload").addClass("disabled");
    var formData = new FormData();
    formData.append('file', $('#file').prop('files')[0]);
    formData.append('name', $("#name").val());
    formData.append('email', $("#email").val());
    formData.append('id', $("#id").val());
    $.ajax({
        url: '/dashboard/admin/investor/uploadFile',
        type: 'POST',
        data: formData,
        processData: false,
        cache: false,
        contentType: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
        },
        success: function( data,status ) {
            console.log(data);
            if(data == 1 && status == 'success'){
                $("#fileUpload").removeClass("disabled");
                Swal.fire({
                    icon:  'success',
                    title: 'Enviado o e-mail com o rendimento para o investidor.',
                })
                }else{
                    $("#fileUpload").removeClass("disabled");
                    Swal.fire({
                        icon:  'error',
                        title: 'Erro ao enviar o e-mail.',
                    })
                }
        },
        error: function (request, status, error) {
            $("#fileUpload").removeClass("disabled");
                    Swal.fire({
                        icon:  'error',
                        title: 'Erro ao enviar o e-mail.',
                    })
        }
    });
});
