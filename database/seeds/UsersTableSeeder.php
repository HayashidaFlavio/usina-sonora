<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name"          => "Flávio Massao Moraes Hayashida",
            "email"         => "fmmh18@gmail.com",
            "password"      => bcrypt("jhmcma130902"),
            "image"         =>    "none",
            "status"        =>    1,
            "permission"    =>    1,
        ]);
    }
}
