<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace'=>'Site'],function(){
    Route::get('/',  'indexController@index')->name('home');
    Route::get('/investidor',  'investorController@index')->name('investor');
    Route::get('/usina/{tag}',  'indexController@factory')->name('factory');
    Route::get('/produto/{tag}',  'indexController@product')->name('product');
    Route::get('/cliente',  'indexController@client')->name('client');
    Route::get('/contato',  'indexController@contact')->name('contact');
    Route::get('/politica',  'indexController@politic')->name('politic');
    Route::get('/trabalhe-conosco',  'indexController@index')->name('working');
    Route::get('/sustentabilidade/{tag}',  'indexController@sustainability')->name('sustainability');
    Route::get('/noticia/detalhe/{id}/{tag}',  'indexController@newsdetail')->name('news.detail');
});

//Route Admin
Route::group(['middleware'=> ['auth'],'namespace'=>'Dashboard'],function(){
  Route::group(['middleware'=> ['can:isAdmin'],'namespace'=>'Admin'],function(){
        Route::group(['middleware'=> ['can:isAdmin'],'namespace'=>'Home'],function(){
            Route::get('/dashboard/admin/home',  'homeController@index')->name('dashboard.admin.home');
        });
        Route::group(['middleware'=> ['can:isAdmin'],'namespace'=>'Page'],function(){
            Route::get('/dashboard/admin/page/{tag}',  'pageController@edit')->name('dashboard.admin.page.edit');
            Route::get('/dashboard/admin/page/{page}/{tag}',  'pageController@edit')->name('dashboard.admin.page.edit.sub');
            Route::post('/dashboard/admin/page/edit',  'pageController@update')->name('dashboard.admin.page.edit');
        });

        Route::group(['middleware'=> ['can:isAdmin'],'namespace'=>'News'],function(){
            Route::get('/dashboard/admin/news/list',  'newsController@index')->name('dashboard.admin.news.list');
            Route::get('/dashboard/admin/news/add',  'newsController@add')->name('dashboard.admin.news.add');
            Route::post('/dashboard/admin/news/add',  'newsController@shore')->name('dashboard.admin.news.add');
            Route::get('/dashboard/admin/news/edit/{id}/{tag}',  'newsController@edit')->name('dashboard.admin.news.edit');
            Route::post('/dashboard/admin/news/edit',  'newsController@update')->name('dashboard.admin.news.edit');
            Route::post('/dashboard/admin/news/status',  'newsController@status')->name('dashboard.admin.news.status');
            Route::post('/dashboard/admin/news/del',  'newsController@delete')->name('dashboard.admin.news.del');
        });

        Route::group(['middleware'=> ['can:isAdmin'],'namespace'=>'Product'],function(){
            Route::get('/dashboard/admin/product/list',  'productController@index')->name('dashboard.admin.product.list');
            Route::get('/dashboard/admin/product/add',  'productController@add')->name('dashboard.admin.product.add');
            Route::post('/dashboard/admin/product/add',  'productController@shore')->name('dashboard.admin.product.add');
            Route::get('/dashboard/admin/product/edit/{id}',  'productController@edit')->name('dashboard.admin.product.edit');
            Route::post('/dashboard/admin/product/edit',  'productController@update')->name('dashboard.admin.product.edit');
            Route::post('/dashboard/admin/product/status',  'productController@status')->name('dashboard.admin.product.status');
            Route::post('/dashboard/admin/product/del',  'productController@delete')->name('dashboard.admin.product.del');
        });

        Route::group(['middleware'=> ['can:isAdmin'],'namespace'=>'User'],function(){
            Route::get('/dashboard/admin/user/list',  'userController@index')->name('dashboard.admin.user.list');
            Route::get('/dashboard/admin/user/add',  'userController@add')->name('dashboard.admin.user.add');
            Route::post('/dashboard/admin/user/add',  'userController@shore')->name('dashboard.admin.user.add');
            Route::get('/dashboard/admin/user/edit/{id}',  'userController@edit')->name('dashboard.admin.user.edit');
            Route::post('/dashboard/admin/user/edit',  'userController@update')->name('dashboard.admin.user.edit');
            Route::post('/dashboard/admin/user/status',  'userController@status')->name('dashboard.admin.user.status');
            Route::post('/dashboard/admin/user/del',  'userController@delete')->name('dashboard.admin.user.del');
        });
        Route::group(['middleware'=> ['can:isAdmin'],'namespace'=>'Investor'],function(){
            Route::get('/dashboard/admin/investor/list',  'investorController@index')->name('dashboard.admin.investor.list');
            Route::get('/dashboard/admin/investor/add',  'investorController@add')->name('dashboard.admin.investor.add');
            Route::post('/dashboard/admin/investor/add',  'investorController@shore')->name('dashboard.admin.investor.add');
            Route::get('/dashboard/admin/investor/edit/{id}',  'investorController@edit')->name('dashboard.admin.investor.edit');
            Route::post('/dashboard/admin/investor/edit',  'investorController@update')->name('dashboard.admin.investor.edit');
            Route::post('/dashboard/admin/investor/del',  'investorController@delete')->name('dashboard.admin.investor.del');
            Route::post('/dashboard/admin/investor/status',  'investorController@status')->name('dashboard.admin.investor.status');
            Route::post('/dashboard/admin/investor/detail',  'investorController@detail')->name('dashboard.admin.investor.detail');
            Route::post('/dashboard/admin/investor/passwordGenerate',  'investorController@passwordGenerate')->name('dashboard.admin.investor.passwordGenerate');
            Route::post('/dashboard/admin/investor/uploadFile',  'investorController@uploadFile')->name('dashboard.admin.investor.uploadFile');
        });

        Route::group(['middleware'=> ['can:isAdmin'],'namespace'=>'Curriculum'],function(){
            Route::get('/dashboard/admin/curriculum/list',  'curriculumController@index')->name('dashboard.admin.curriculum.list');
        });

        Route::group(['middleware'=> ['can:isAdmin'],'namespace'=>'Parameter'],function(){
            Route::get('/dashboard/admin/parameter/email',  'ParameterController@email')->name('dashboard.admin.parameter.email');
            Route::get('/dashboard/admin/parameter/company',  'ParameterController@company')->name('dashboard.admin.parameter.company');
            Route::get('/dashboard/admin/parameter/lgpd',  'ParameterController@lgpd')->name('dashboard.admin.parameter.lgpd');
            Route::post('/dashboard/admin/parameter/email/edit',  'ParameterController@update')->name('dashboard.admin.parameter.email.edit');
            Route::post('/dashboard/admin/parameter/company/edit',  'ParameterController@update')->name('dashboard.admin.parameter.company.edit');
            Route::post('/dashboard/admin/parameter/lgpd/edit',  'ParameterController@update')->name('dashboard.admin.parameter.lgpd.edit');
            Route::get('/dashboard/admin/parameter/building/list',  'ParameterController@listBuilding')->name('dashboard.admin.parameter.building.list');
            Route::get('/dashboard/admin/parameter/building/add',  'ParameterController@addBuilding')->name('dashboard.admin.parameter.building.add');
            Route::get('/dashboard/admin/parameter/building/edit/{id}',  'ParameterController@editBuilding')->name('dashboard.admin.parameter.building.edit');
            Route::post('/dashboard/admin/parameter/building/edit',  'ParameterController@updateBuilding')->name('dashboard.admin.parameter.building.edit');
            Route::post('/dashboard/admin/parameter/building/del',  'ParameterController@delBuilding')->name('dashboard.admin.parameter.building.del');
        });


        Route::group(['middleware'=> ['can:isAdmin'],'namespace'=>'Contact'],function(){
            Route::get('/dashboard/admin/contact/list',  'contactController@index')->name('dashboard.admin.contact.list');
            Route::get('/dashboard/admin/contact/add',  'contactController@add')->name('dashboard.admin.contact.add');
            Route::post('/dashboard/admin/contact/add',  'contactController@shore')->name('dashboard.admin.contact.add');
            Route::get('/dashboard/admin/contact/edit/{id}',  'contactController@edit')->name('dashboard.admin.contact.edit');
            Route::post('/dashboard/admin/contact/edit',  'contactController@update')->name('dashboard.admin.contact.edit');
            Route::post('/dashboard/admin/contact/del',  'contactController@delete')->name('dashboard.admin.contact.del');
        });
    });
});


Route::group(['middleware'=> ['auth'],'namespace'=>'Dashboard'],function(){
    Route::group(['middleware'=> ['can:isInvestor'],'namespace'=>'Investor'],function(){
        Route::group(['middleware'=> ['can:isInvestor'],'namespace'=>'Home'],function(){
            Route::get('/dashboard/investor/home',  'homeController@index')->name('dashboard.investor.home');
        });

        Route::group(['middleware'=> ['can:isInvestor'],'namespace'=>'Investor'],function(){
            Route::post('/dashboard/investor/edit',  'investorController@update')->name('dashboard.investor.edit');
            Route::get('/dashboard/investor/list',  'investorController@list')->name('dashboard.investor.list');
        });
    });
});
Auth::routes();

