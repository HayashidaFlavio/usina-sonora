<?php

namespace App\Http\Controllers\dashboard\admin\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class userController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {

        $users = new User;

        $data = $users::where('role','!=','investor')->Paginate(20);

        return view('dashboard.admin.user.list',array(
            'datas' =>  $data
        ));
    }

    public function add(Request $request)
    {
        return view('dashboard.admin.user.form',array(
            'data'          =>  0,
            'button_action' => 'Adicionar',
            'button_icon'   => 'fas fa-plus',
            'form_action'   => 'dashboard.admin.user.add'
        ));
    }

    public function edit(Request $request)
    {
        $users = new User;

        if(empty($request->id)){

            return Redirect::route('dashboard.admin.user.list')->with(['icon' => 'error','message' => 'Id da notícia está vazio']);

        }
        $data = $users::find($request->id);

        return view('dashboard.admin.user.form',array(
            'data'          =>  $data,
            'button_action' => 'Editar',
            'button_icon'   => 'fas fa-edit',
            'form_action'   => 'dashboard.admin.user.edit'
        ));
    }

    public function shore(Request $request)
    {
        $validator = $request->validate([
            'name'     => 'required',
            'email'     => 'required',
            'password'     => 'required',
        ]);

        if(empty($validator))
        {
            return $request->input();
        }

        if($request->status == 'on'){
            $status = 1;
        }else{
            $status = 0;
        }

        $users = new User;

        $users->title    = $request->title;
        $users->subtitle = $request->subtitle;
        $users->date     = $request->date;
        $users->content  = $request->content;
        $users->tag      = $request->tag;
        $users->status   = $status;

        if($users->save() > 0)
        {
            return Redirect::to('/dashboard/admin/user/add')->with(['icon' => 'success','message' => 'Dados Salvo']);
        }else{
            return Redirect::to('/dashboard/admin/user/add')->with(['icon' => 'error','message' => 'Erro ao salvar']);
        }

    }

    public function update(Request $request)
    {
        $validator = $request->validate([
            'name'     => 'required',
            'email'     => 'required',
            'password'     => 'required',
        ]);

        if(empty($validator))
        {
            return $request->input();
        }

        if($request->status == 'on'){
            $status = 1;
        }else{
            $status = 0;
        }

        $users = new User;

        $user = $users::find($request->id);

        $user->title    = $request->title;
        $user->subtitle = $request->subtitle;
        $user->date     = $request->date;
        $user->content  = $request->content;
        $user->tag      = $request->tag;
        $user->status   = $status;

        if($user->save() > 0)
        {
            return Redirect::to('/dashboard/admin/user/edit/'.$request->id)->with(['icon' => 'success','message' => 'Dados Atualizado']);
        }else{
            return Redirect::to('/dashboard/admin/user/edit/'.$request->id)->with(['icon' => 'error','message' => 'Erro ao atualizar']);
        }
    }
    public function delete(Request $request)
    {
        $data =  User::where('id',$request->id)->update([
            'status'    => 0,
            'updated_at'=> date('Y-m-d H:i:s')
        ]);
        return $data;
    }
    public function status(Request $request)
    {
        $data =  User::where('id',$request->id)->update([
            'status'    => $request->stats,
            'updated_at'=> date('Y-m-d H:i:s')
        ]);
        return $data;
    }
}
