<?php

namespace App\Http\Controllers\dashboard\admin\parameter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Parameter;
use App\Models\Departament;
use Illuminate\Support\Facades\Redirect;

class parameterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function email(Request $request)
    {

            $data  = Parameter::find(1);

            return view('dashboard.admin.parameter.email.form',array(
                'data'          =>  $data,
                'form_action'   => 'dashboard.admin.parameter.email.edit',
                'type'          => 'email',
                'button_action' => 'Editar',
                'button_icon'   => 'fas fa-edit'
            ));
    }
    public function lgpd(Request $request)
    {

            $data  = Parameter::find(1);

            return view('dashboard.admin.parameter.lgpd.form',array(
                'data'          =>  $data,
                'form_action'   => 'dashboard.admin.parameter.lgpd.edit',
                'type'          => 'lgpd',
                'button_action' => 'Editar',
                'button_icon'   => 'fas fa-edit'
            ));
    }
    public function company(Request $request)
    {
            $data  = Company::find(1);

            return view('dashboard.admin.parameter.company.form',array(
                'data'          =>  $data,
                'form_action'   => 'dashboard.admin.parameter.company.edit',
                'type'          => 'company',
                'button_action' => 'Editar',
                'button_icon'   => 'fas fa-edit'
            ));
    }

    public function update(Request $request)
    {
        if($request->type == 'company')
        {
            $company = Company::find($request->id);

            $company->name        = $request->name;
            $company->whatsapp    = $request->whatsapp;
            $company->fax         = $request->fax;
            $company->phone       = $request->phone;
            $company->address     = $request->address;
            $company->maps        = $request->maps;
            $company->facebook    = $request->facebook;
            $company->linkedin    = $request->linkedin;
            $company->instagram   = $request->instagram;
            $company->googleplus  = $request->googleplus;
            $company->updated_at  = date('Y-m-d H:i:s');

            if($company->save() > 0)
            {
                return Redirect::to('dashboard/admin/parameter/company')->with(['icon' => 'success','message' => 'Dados Atualizado']);
            }else{
                return Redirect::to('dashboard/admin/parameter/company')->with(['icon' => 'success','message' => 'Dados Atualizado']);
            }

        }elseif($request->type == 'email')
        {
            $parameter = Parameter::find($request->id);
            if(!empty($request->email_investor))
            $parameter->content_email_investor                   = $request->email_investor;
            if(!empty($request->email_contact))
            $parameter->content_email_contact_answer             = $request->email_contact;
            if(!empty($request->email_password_generate))
            $parameter->content_email_investor_password_generate = $request->email_password_generate;

            if($parameter->save() > 0)
            {
                return Redirect::to('dashboard/admin/parameter/email')->with(['icon' => 'success','message' => 'Dados Atualizado']);
            }else{
                return Redirect::to('dashboard/admin/parameter/email')->with(['icon' => 'success','message' => 'Dados Atualizado']);
            }


        }elseif($request->type == 'lgpd')
        {
            $parameter = Parameter::find($request->id);

            $parameter->content_lgpd_cookie     = $request->cookie_lgpd;
            $parameter->content_lgpd_text       = $request->text_lgpd;

            if($parameter->save() > 0)
            {
                return Redirect::to('dashboard/admin/parameter/lgpd')->with(['icon' => 'success','message' => 'Dados Atualizado']);
            }else{
                return Redirect::to('dashboard/admin/parameter/lgpd')->with(['icon' => 'success','message' => 'Dados Atualizado']);
            }
        }
    }

    public function listBuilding()
    {
        $data = Departament::all();

        return view('dashboard.admin.parameter.building.list',array(
            'datas' =>  $data
        ));
    }
    public function addBuilding()
    {
        return view('dashboard.admin.parameter.building.form',array(
            'data'          =>  0,
            'button_action' => 'Adicionar',
            'button_icon'   => 'fas fa-plus',
            'form_action'   => 'dashboard.admin.parameter.building.add'
        ));

    }

    public function editBuilding(Request $request)
    {
        $data = Departament::find($request->id);

        return view('dashboard.admin.parameter.building.form',array(
            'data'          =>  $data,
            'button_action' => 'Editar',
            'button_icon'   => 'fas fa-edit',
            'form_action'   => 'dashboard.admin.parameter.building.edit'
        ));
    }

    public function delBuilding(Request $request)
    {

    }
}
