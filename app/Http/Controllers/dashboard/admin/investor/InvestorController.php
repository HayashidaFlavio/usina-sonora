<?php

namespace App\Http\Controllers\dashboard\admin\investor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use App\Models\Parameter;
use App\Document;
use Illuminate\Support\Facades\Mail;

class InvestorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $users = new User;

        $data = $users::where('role','investor')->Paginate(20);

        return view('dashboard.admin.investor.list',array(
            'datas' =>  $data
        ));
    }

    public function add(Request $request)
    {
        return view('dashboard.admin.investor.form',array(
            'data'          =>  0,
            'button_action' => 'Adicionar',
            'button_icon'   => 'fas fa-plus',
            'form_action'   => 'dashboard.admin.investor.add'
        ));
    }

    public function edit(Request $request)
    {
        $users = new User;

        if(empty($request->id)){

            return Redirect::route('dashboard.admin.investor.list')->with(['icon' => 'error','message' => 'Id da notícia está vazio']);

        }
        $data = $users::find($request->id);

        return view('dashboard.admin.investor.form',array(
            'data'          =>  $data,
            'button_action' => 'Editar',
            'button_icon'   => 'fas fa-edit',
            'form_action'   => 'dashboard.admin.investor.edit'
        ));
    }

    public function shore(Request $request)
    {
        $validator = $request->validate([
            'name'     => 'required',
            'email'     => 'required',
            'password'     => 'required',
        ]);

        if(empty($validator))
        {
            return $request->input();
        }

        if($request->status == 'on'){
            $status = 1;
        }else{
            $status = 0;
        }

        $users = new User;

        $users->name            = $request->name;
        $users->enterprise      = $request->enterprise;
        $users->relationship    = $request->relationship;
        $users->email           = $request->email;
        $users->phone           = $request->phone;
        $users->password        = bcrypt($request->password);
        $users->created_at      = date('Y-m-d H:i:s');
        $users->status          = $status;
        $users->role            = 2;

        if($users->save() > 0)
        {
            return Redirect::to('/dashboard/admin/investor/add')->with(['icon' => 'success','message' => 'Dados Salvo']);
        }else{
            return Redirect::to('/dashboard/admin/investor/add')->with(['icon' => 'error','message' => 'Erro ao salvar']);
        }

    }

    public function update(Request $request)
    {
        $users = new User;
        $validator = $request->validate([
            'name'     => 'required',
            'email'     => 'required',
            'password'     => 'required',
        ]);

        if(empty($validator))
        {
            return $request->input();
        }

        if($request->status == 'on'){
            $status = 1;
        }else{
            $status = 0;
        }

        $user = ([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password
            ]);

        Mail::send(new \App\Mail\investorPassword($user));

        $user = $users::find($request->id);

        $user->name            = $request->name;
        $user->enterprise      = $request->enterprise;
        $user->relationship    = $request->relationship;
        $user->email           = $request->email;
        $user->phone           = $request->phone;
        $user->password        = bcrypt($request->password);
        $user->updated_at      = date('Y-m-d H:i:s');
        $user->status          = $status;
        $user->role            = 'investor';

        if($user->save() > 0)
        {
            return Redirect::to('/dashboard/admin/investor/edit/'.$request->id)->with(['icon' => 'success','message' => 'Dados Atualizado']);
        }else{
            return Redirect::to('/dashboard/admin/investor/edit/'.$request->id)->with(['icon' => 'error','message' => 'Erro ao atualizar']);
        }
    }
    public function delete(Request $request)
    {
        $data =  User::where('id',$request->id)->update([
            'status'    => 0,
            'updated_at'=> date('Y-m-d H:i:s')
        ]);
        return $data;
    }
    public function detail(Request $request)
    {
        $data =  User::find($request->id);
        return $data;
    }

    public function status(Request $request)
    {
        $data =  User::where('id',$request->id)->update([
            'status'    => $request->stats,
            'updated_at'=> date('Y-m-d H:i:s')
        ]);
        return $data;
    }

    public function passwordGenerate(Request $request)
    {

            $param = Parameter::find(1);

            $user = ([
            'name'          => $request->name,
            'email'         => $request->email,
            'password'      => $request->password,
            'email_content' => $param->content_email_investor_password_generate,
            ]);
            $data= User::where('id',$request->id)->update([
                'password'      => bcrypt($request->password),
                'updated_at'    => date('Y-m-d H:i:s')
            ]);

            Mail::send(new \App\Mail\investorPassword($user));
        return $data;
    }
    public function uploadFile(Request $request)
    {
        $param = Parameter::find(1);


        $nameFile = null;

        // Define um aleatório para o arquivo baseado no timestamps atual
        $name = uniqid(date('HisYmd'));

        // Recupera a extensão do arquivo
        $extension = $request->file->extension();

        // Define finalmente o nome
        $nameFile = "{$name}.{$extension}";
        if ($request->hasFile('file') && $request->file('file')->isValid()) {



            // Faz o upload:
            $upload = $request->file->storeAs('documents/investor/'.$request->id, $nameFile,'public');
            // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with(['icon' => 'error','message' =>'error', 'Falha ao fazer upload'])
                            ->withInput();

        }
            $link_download = '/documents/investor/'.$request->id.'/'.$nameFile;
            $user = ([
            'name'          => $request->name,
            'email'         => $request->email,
            'email_content' => $param->content_email_investor,
            'url'           => $link_download
            ]);
            $data = new Document;
            $data->user_id = $request->id;
            $data->document = $nameFile;
            $data->reference = date('Y-m-d');

            Mail::send(new \App\Mail\InvestorFileDocument($user));
            return  $data->save();;
    }
}
