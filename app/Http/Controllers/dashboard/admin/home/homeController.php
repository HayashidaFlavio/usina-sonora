<?php

namespace App\Http\Controllers\dashboard\admin\home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;
use App\User;

class homeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $total_news = News::count();
        $total_users = User::where('role','investor')->count();
        return view('dashboard.admin.home.index',['total_news' => $total_news, 'total_users'=> $total_users]);
    }
}
