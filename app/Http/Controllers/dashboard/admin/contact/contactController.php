<?php

namespace App\Http\Controllers\dashboard\admin\contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class contactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $contacts = new Contact;

        $contact = $contacts::Paginate(20);

        return view('dashboard.admin.contact.list',array(
            'datas' =>  $contact
        ));
    }
    public function add(Request $request)
    {
        return view('dashboard.admin.contact.form',array(
            'data'          =>  0,
            'button_action' => 'Adicionar',
            'button_icon'   => 'fas fa-plus',
            'form_action'   => 'dashboard.contact.add'
        ));
    }
    public function edit(Request $request)
    {
        $contacts = new Contact;

        if(empty($request->id)){

            return Redirect::route('dashboard.admin.contact.list')->with(['icon' => 'error','message' => 'Id da notícia está vazio']);

        }
        $data = $contacts::find($request->id);

        return view('dashboard.admin.contact.form',array(
            'data'          =>  $data,
            'button_action' => 'Editar',
            'button_icon'   => 'fas fa-edit',
            'form_action'   => 'dashboard.admin.contact.edit'
        ));
    }

    public function shore(Request $request)
    {
        $validator = $request->validate([
            'name'     => 'required',
            'email'   => 'required',
            'content'   => 'required'
        ]);

        if(empty($validator))
        {
            return $request->input();
        }
        $contacts = new Contact;

        $contacts->name      = $request->name;
        $contacts->email     = $request->email;
        $contacts->content   = $request->content;
        $contacts->status    = 1;

        if($contacts->save() > 0)
        {
            return Redirect::to('/dashboard/contact/add')->with(['icon' => 'success','message' => 'Dados Salvo']);
        }else{
            return Redirect::to('/dashboard/contact/add')->with(['icon' => 'error','message' => 'Erro ao salvar']);
        }

    }

    public function update(Request $request)
    {
        $validator = $request->validate([
            'name'     => 'required',
            'email'      => 'required',
            'content'   => 'required'
        ]);

        if(empty($validator))
        {
            return $request->input();
        }
        $contacts = new Contact;

        $contact = $contacts::find($request->id);

        $contact->name      = $request->name;
        $contact->email     = $request->email;
        $contact->content   = $request->content;

        if($contact->save() > 0)
        {
            return Redirect::to('/dashboard/contact/edit'.$request->id)->with(['icon' => 'success','message' => 'Dados Atualizado']);
        }else{
            return Redirect::to('/dashboard/contact/edit/'.$request->id)->with(['icon' => 'error','message' => 'Erro ao atualizar']);
        }
    }
    public function delete(Request $request)
    {
        $data =  Contact::where('id',$request->id)->update([
            'status'    => 0,
            'updated_at'=> date('Y-m-d H:i:s')
        ]);
        return $data;
    }
    public function status(Request $request)
    {
        $data =  Contact::where('id',$request->id)->update([
            'status'    => $request->stats,
            'updated_at'=> date('Y-m-d H:i:s')
        ]);
        return $data;
    }
}
