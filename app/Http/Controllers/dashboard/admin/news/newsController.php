<?php

namespace App\Http\Controllers\dashboard\admin\news;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class newsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $news = new News;

        $new = $news::Paginate(20);

        return view('dashboard.admin.news.list',array(
            'datas' =>  $new
        ));
    }

    public function add(Request $request)
    {
        return view('dashboard.admin.news.form',array(
            'data'          =>  0,
            'button_action' => 'Adicionar',
            'button_icon'   => 'fas fa-plus',
            'form_action'   => 'dashboard.admin.news.add'
        ));
    }

    public function edit(Request $request)
    {
        $news = new News;

        if(empty($request->id)){

            return Redirect::route('dashboard.admin.news.list')->with(['icon' => 'error','message' => 'Id da notícia está vazio']);

        }
        $new = $news::find($request->id);

        return view('dashboard.admin.news.form',array(
            'data'          =>  $new,
            'button_action' => 'Editar',
            'button_icon'   => 'fas fa-edit',
            'form_action'   => 'dashboard.admin.news.edit'
        ));
    }

    public function shore(Request $request)
    {
        $nameFile = null;

        // Define um aleatório para o arquivo baseado no timestamps atual
        $name = uniqid(date('HisYmd'));

        // Recupera a extensão do arquivo
        $extension = $request->image->extension();

        // Define finalmente o nome
        $nameFile = "{$name}.{$extension}";

        $validator = $request->validate([
            'title'     => 'required',
            'date'      => 'required',
            'content'   => 'required'
        ]);

        if($request->publish == 'on'){
            $publish = 1;
        }else{
            $publish = 0;
        }

        if($request->status == 'on'){
            $status = 1;
        }else{
            $status = 0;
        }

        if(empty($validator))
        {
            return $request->input();
        }
        $news = new News;

        $news->title    = $request->title;
        $news->subtitle = $request->subtitle;
        $news->date     = implode('-', array_reverse(explode('/',$request->date)));
        $news->content  = $request->content;
        $news->publish  = $publish;
        $news->author   = $request->author;
        $news->url_site = $request->url_site;
        $news->image    = $nameFile;
        $news->tag      = $request->tag;
        $news->status   = $status;
        $result = $news->save();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {



            // Faz o upload:
            $upload = $request->image->storeAs('news/'.$news->id, $nameFile,'public');
            // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with(['icon' => 'error','message' =>'error', 'Falha ao fazer upload'])
                            ->withInput();

        }


        if($result > 0)
        {
            return Redirect::to('/dashboard/admin/news/add')->with(['icon' => 'success','message' => 'Dados Salvo']);
        }else{
            return Redirect::to('/dashboard/admin/news/add')->with(['icon' => 'error','message' => 'Erro ao salvar']);
        }

    }

    public function update(Request $request)
    {
        $nameFile = null;

        // Define um aleatório para o arquivo baseado no timestamps atual
        $name = uniqid(date('HisYmd'));

        // Recupera a extensão do arquivo
        $extension = $request->image->extension();

        // Define finalmente o nome
        $nameFile = "{$name}.{$extension}";

        $validator = $request->validate([
            'title'     => 'required',
            'date'      => 'required',
            'content'   => 'required'
        ]);

        if(empty($validator))
        {
            return $request->input();
        }
        $news = new News;

        $new = $news::find($request->id);

        if($request->publish == 'on'){
            $publish = 1;
        }else{
            $publish = 0;
        }

        if($request->status == 'on'){
            $status = 1;
        }else{
            $status = 0;
        }


        $new->title     = $request->title;
        $new->subtitle  = $request->subtitle;
        $new->date      = implode('-', array_reverse(explode('/',$request->date)));
        $new->content   = $request->content;
        $new->publish   = $publish;
        $new->image     = $nameFile;
        $news->author   = $request->author;
        $news->url_site = $request->url_site;
        $new->tag       = $request->tag;
        $new->status    = $status;

        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            //Se Existir imagem e deletado.
            $old = 'news/'.$request->id.'/'.$request->imageOld;
            Storage::disk('public')->delete($old);


            // Faz o upload:
            $upload = $request->image->storeAs('news/'.$request->id, $nameFile,'public');
            // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with(['icon' => 'error','message' =>'error', 'Falha ao fazer upload'])
                            ->withInput();

        }

        if($new->save() > 0)
        {
            return Redirect::to('/dashboard/admin/news/edit/'.$request->id.'/'.$request->tag)->with(['icon' => 'success','message' => 'Dados Atualizado']);
        }else{
            return Redirect::to('/dashboard/admin/news/edit/'.$request->id.'/'.$request->tag)->with(['icon' => 'error','message' => 'Erro ao atualizar']);
        }
    }
    public function delete(Request $request)
    {
        $data =  News::where('id',$request->id)->update([
            'status'    => 0,
            'updated_at'=> date('Y-m-d H:i:s')
        ]);
        return $data;
    }
    public function status(Request $request)
    {
        $data =  News::where('id',$request->id)->update([
            'status'    => $request->stats,
            'updated_at'=> date('Y-m-d H:i:s')
        ]);
        return $data;
    }
}
