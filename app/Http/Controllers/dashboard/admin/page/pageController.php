<?php

namespace App\Http\Controllers\dashboard\admin\page;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class pageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
          return view('dashboard.admin.page.list');
    }

    public function add()
    {
        return view('dashboard.admin.page.form');
    }
    public function edit(Request $request)
    {
        $pages = new Page;

        $page  = $pages::where('tag',$request->tag)->first();
        if(!empty($request->page))
        {
            return view('dashboard.admin.page.form',array(
                'data' =>  $page,
                'form_action'   => 'edit',
                'button_action' => 'Editar',
                'button_icon'   => 'fas fa-edit',
                'page'          => $request->page
            ));
        }else{
            return view('dashboard.admin.page.form',array(
                'data' =>  $page,
                'form_action'   => 'edit',
                'button_action' => 'Editar',
                'button_icon'   => 'fas fa-edit'
            ));
        }

    }

    public function update(Request $request)
    {
        $pages = new Page;

        $page = $pages::find($request->id);

        if($request->status == 'on'){
            $status = 1;
        }else{
            $status = 0;
        }

        $page->content = $request->content;
        $page->status  = $status;
        if($page->save() > 0)
        {
            if(!empty($request->page))
            {
                return Redirect::to('dashboard/admin/page/'.$request->page.'/'.$request->tag)->with(['icon' => 'success','message' => 'Dados Atualizado']);
            }else{
                return Redirect::to('dashboard/admin/page/'.$request->tag)->with(['icon' => 'success','message' => 'Dados Atualizado']);
            }
        }else{
            if(!empty($request->page))
            {
                return Redirect::to('dashboard/admin/page/'.$request->page.'/'.$request->tag)->with(['icon' => 'error','message' => 'Erro ao atualizar']);
            }else{
                return Redirect::to('dashboard/admin/page/'.$request->tag)->with(['icon' => 'error','message' => 'Erro ao atualizar']);
            }
        }


    }
}
