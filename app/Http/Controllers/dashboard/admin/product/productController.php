<?php

namespace App\Http\Controllers\dashboard\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class productController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $products = new Product;

        $product = $products::Paginate(20);

        return view('dashboard.admin.product.list',array(
            'datas' =>  $product
        ));
    }
    public function add(Request $request)
    {
        return view('dashboard.admin.product.form',array(
            'data'          =>  0,
            'button_action' => 'Adicionar',
            'button_icon'   => 'fas fa-plus',
            'form_action'   => 'dashboard.admin.product.add'
        ));
    }
    public function edit(Request $request)
    {
        $products = new Product;

        if(empty($request->id)){

            return Redirect::route('dashboard.admin.product.list')->with(['icon' => 'error','message' => 'Id da notícia está vazio']);

        }
        $product = $products::find($request->id);

        return view('dashboard.admin.product.form',array(
            'data'          =>  $product,
            'button_action' => 'Editar',
            'button_icon'   => 'fas fa-edit',
            'form_action'   => 'dashboard.admin.product.edit'
        ));
    }

    public function shore(Request $request)
    {
         $validator = $request->validate([
            'title'     => 'required',
            'content'   => 'required'
        ]);

        if(empty($validator))
        {
            return $request->input();
        } $validator = $request->validate([
            'title'     => 'required',
            'content'   => 'required'
        ]);

        if(empty($validator))
        {
            return $request->input();
        }

        if($request->status == 'on'){
            $status = 1;
        }else{
            $status = 0;
        }

        $products = new Product;
        $products->title    = $request->title;
        $products->content  = $request->content;
        $products->tag      = $request->tag;
        if(!empty($nameFile)){
        $products->image    =  $nameFile;
        }
        $products->status   = $status;
        $result = $products->save();


        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            $nameFile = null;

            // Define um aleatório para o arquivo baseado no timestamps atual
            $name = uniqid(date('HisYmd'));

            // Recupera a extensão do arquivo
            $extension = $request->image->extension();

            // Faz o upload:
            $upload = $request->image->storeAs('products/'.$products->id, $nameFile,'public');
            // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with(['icon' => 'error','message' =>'error', 'Falha ao fazer upload'])
                            ->withInput();

        }

        if($result > 0)
        {
            return Redirect::to('/dashboard/admin/product/add')->with(['icon' => 'success','message' => 'Dados Salvo']);
        }else{
            return Redirect::to('/dashboard/admin/product/add')->with(['icon' => 'error','message' => 'Erro ao salvar']);
        }

    }

    public function update(Request $request)
    {
        $validator = $request->validate([
            'title'     => 'required',
            'content'   => 'required'
        ]);

        if(empty($validator))
        {
            return $request->input();
        }

        if($request->status == 'on'){
            $status = 1;
        }else{
            $status = 0;
        }


        $products = new Product;
        $product = $products::find($request->id);

        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            //Se Existir imagem e deletado.
            $old = 'products/'.$request->id.'/'.$request->imageOld;
            Storage::disk('public')->delete($old);

            $nameFile = null;

            // Define um aleatório para o arquivo baseado no timestamps atual
            $name = uniqid(date('HisYmd'));

            // Recupera a extensão do arquivo
            $extension = $request->image->extension();

            // Faz o upload:
            $upload = $request->image->storeAs('products/'.$request->id, $nameFile,'public');
            // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with(['icon' => 'error','message' =>'error', 'Falha ao fazer upload'])
                            ->withInput();

        }

        $product->title         = $request->title;
        $product->content       = $request->content;
        $product->tag           = $request->tag;
        if(!empty($nameFile))
        {
        $products->image        =  $nameFile;
        }
        $product->status        = $status;
        $product->updated_at    = date('Y-m-d H:i:s');

        if($product->save() > 0)
        {
            return Redirect::to('/dashboard/admin/product/edit/'.$request->id)->with(['icon' => 'success','message' => 'Dados Atualizado']);
        }else{
            return Redirect::to('/dashboard/admin/product/edit/'.$request->id)->with(['icon' => 'error','message' => 'Erro ao atualizar']);
        }
    }
    public function delete(Request $request)
    {
        $data =  Product::where('id',$request->id)->update([
            'status'    => 0,
            'updated_at'=> date('Y-m-d H:i:s')
        ]);
        return $data;
    }
    public function status(Request $request)
    {
        $data =  Product::where('id',$request->id)->update([
            'status'    => $request->stats,
            'updated_at'=> date('Y-m-d H:i:s')
        ]);
        return $data;
    }
}
