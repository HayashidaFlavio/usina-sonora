<?php

namespace App\Http\Controllers\dashboard\investor\investor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Document;

class investorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(Request $request)
    {
        $users = new User;

        $data = $users::find($request->id);

        return view('dashboard.investor.user.form',array(
            'data'          =>  $data,
            'button_action' => 'Editar',
            'button_icon'   => 'fas fa-edit',
            'form_action'   => 'dashboard.admin.user.edit'
        ));
    }
    public function list(Request $request)
    {
        //echo Auth::user()->id;
        $data = Document::where('user_id',Auth::user()->id)->get();
        return view('dashboard.investor.document.list',array(
            'datas'          =>  $data));

    }
}
