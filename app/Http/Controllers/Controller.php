<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;
use App\Models\Company;
use App\Models\Product;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $company = company::find(1);
        $products = Product::all(['title','tag']);

        View::share('facebook', $company->facebook);
        View::share('maps', $company->maps);
        View::share('linkedin', $company->linkedin);
        View::share('googleplus', $company->googleplus);
        View::share('instagram', $company->instagram);
        View::share('address', $company->address);
        View::share('name', $company->name);
        View::share('fax', $company->fax);
        View::share('phone', $company->phone);
        View::share('products', $products);
    }
}
