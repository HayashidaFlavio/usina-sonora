<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Document;


class investorController extends Controller
{
    public function index()
    {
        $documents = Document::all();


        return view('site.investor.index',['documents'=>$documents]);
    }

    public function shore(Request $request)
    {
        $validator = $request->validate([
            'name'     => 'required',
            'email'     => 'required',
            'password'     => 'required',
        ]);

        if(empty($validator))
        {
            return $request->input();
        }

        if($request->status == 'on'){
            $status = 1;
        }else{
            $status = 0;
        }

        $users = new User;

        $users->name            = $request->name;
        $users->enterprise      = $request->enterprise;
        $users->relationship    = $request->relationship;
        $users->email           = $request->email;
        $users->phone           = $request->phone;
        $users->created_at      = date('Y-m-d H:i:s');
        $users->status          = 0;

        if($users->save() > 0)
        {
            return Redirect::to('/dashboard/admin/investor/add')->with(['icon' => 'success','message' => 'Dados Salvo']);
        }else{
            return Redirect::to('/dashboard/admin/investor/add')->with(['icon' => 'error','message' => 'Erro ao salvar']);
        }

    }
}
