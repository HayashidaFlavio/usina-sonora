<?php

namespace App\Http\Controllers\site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Product;
use App\Models\News;

class indexController extends Controller
{
    public function index(Request $request)
    {

            $news = News::limit(6)->orderBy('date','desc')->get();

            return view('site.home.index',['news' => $news]);

    }

    public function factory($tag)
    {
        $data = Page::where('tag',$tag)->first();
        return view('site.factory.index',['data' => $data]);
    }

    public function product($tag)
    {
        $data = Product::where('tag',$tag)->first();
        return view('site.product.index',['data' => $data]);
    }

    public function sustainability($tag)
    {
        $data = Page::where('tag',$tag)->first();
        return view('site.sustainability.index',['data' => $data]);
    }
    public function client()
    {
        $data = Page::where('tag','cliente')->first();
        return view('site.client.index',['data' => $data]);
    }

    public function politic()
    {
        return view('site.politic.index');

    }

    public function newsdetail($id)
    {

        $news = News::find($id);
        $news->view += 1;
        $news->save();
        return view('site.news.detail',['new' => $news]);
    }
}
