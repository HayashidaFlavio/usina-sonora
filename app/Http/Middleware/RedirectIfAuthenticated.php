<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {

            if(Auth::user()->hasRole('Admin'))
            {
                return Redirect::to('/dashboard/admin/home');

            }else if(Auth::user()->hasRole('Investor'))
            {

                return Redirect::to('/dashboard/investor/home');

            }
        }

        return $next($request);
    }
}
