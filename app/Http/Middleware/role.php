<?php

namespace App\Http\Middleware;
use Auth;

use Closure;

class role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Auth::check()) {
            return redirect()->route('login');
        }

        $user = Auth::user();
        if($user->hasRole('Admin')){
            return $next($request);
        }
        if($user->hasRole('Investor')){
            return $next($request);
        }
        return false;
    }
}
