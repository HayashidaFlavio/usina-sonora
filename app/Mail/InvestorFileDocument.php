<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\Models\Parameter;

class InvestorFileDocument extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        //
        $this->user = $user;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Informe de rendimentos do seu investimento.');
        $this->to($this->user['email'],$this->user['name']);
        return $this->markdown('mail.investor.InvestorFileDocument',['data' => $this->user,'url'=> $this->user['url']]);
    }
}
