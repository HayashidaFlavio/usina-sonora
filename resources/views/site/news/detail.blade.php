@extends('layouts.masterpagedetail')

@section('content')
<div class="col-md-12 text-right">
<ol class="breadcrumb" style="background-color: transparent">
    <li><a href="{{ url('') }}"><i class="fas fa-home"></i></a>&nbsp;</li>
    <li>> Notícia </li>
    <li class="text-muted">> {{ $new->title }}</li>
</ol>

</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-right">
            <span class="text-muted"><b>Compartilhe nas redes sociais: </b></span>
            <a href="https://facebook.com/sharer/sharer.php?u={{ url('/noticia/detalhe/'.$new->id."/".$new->tag)}}&t={{$new->title}}" class="btn btn-primary" ><i class="fab fa-facebook-f text-white"></i></a>
            <a href="https://linkedin.com/shareArticle?mini=true&url={{ url('/noticia/detalhe/'.$new->id."/".$new->tag)}}" class="btn btn-primary sr-only" ><i class="fab fa-linkedin-in text-white"></i></a>
            <a href="https://api.whatsapp.com/send?text={{ url('/noticia/detalhe/'.$new->id."/".$new->tag)}}" class="btn btn-success" ><i class="fab fa-whatsapp text-white"></i></a>
            <a href="https://twitter.com/intent/tweet?text={{ $new->title }}&url={{ url('/noticia/detalhe/'.$new->id."/".$new->tag)}}" class="btn btn-info" ><i class="fab fa-twitter text-white"></i></a>
              <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center p-2"><h2>{{ $new->title }}</h2></div>
        @if(!empty($new->subtitle))
        <div class="col-md-12 text-center p-4"><h5 class="text-muted">{{ $new->subtitle }}</h5></div>
        @endif
        <p><img  class="img-thumbnail mx-auto d-block" style="border: none" src="https://guiadoestudante.abril.com.br/wp-content/uploads/sites/4/2020/03/cursos-online-1.jpg" alt="">
        </p>
        <p><?php echo $new->content; ?></p>
    </div>
</div>
@endsection
