@extends('layouts.masterpage')
@section('content')
<div class="col-md-12 tarja">
    <div class="row">
        <div class="col-md-10 offset-md-1 p-5 sub-header text-white">
            <h2>Sustentabilidade</h2>
            <hr width="90%" align="left">
            <h4>{{ $data->name }}</h4>
        </div>
    </div>
</div>
<div class="container p-5">
    <div class="row">
        <div class="col-md-12">
            <?php echo $data->content; ?>
        </div>
    </div>
</div>
@endsection
