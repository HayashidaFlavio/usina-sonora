@extends('layouts.masterpage')
@section('css')
<link type="text/css" rel="stylesheet" href="{{ url('assets/css/timeline.css') }}" />
@endsection
@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-md-12 text-center mb-5"><h2>Conheça a nossa Política do sistema de gestão integrado</h2></div>
        <div class="col-md-12">
            <ul class="timeline">
                <li>
                  <div class="timeline-badge success"><i class="fas fa-utensils"></i></div>
                  <div class="timeline-panel">
                    <div class="timeline-heading">
                      <h4 class="timeline-title">Fabricar Produtos Seguros</h4>
                     </div>
                    <div class="timeline-body">
                        <p>Fabricar produtos seguros de forma econômica e racional, atendendo a necessidades dos clientes e de acordo com as normas de Segurança de Alimentos.</p>
                    </div>
                  </div>
                </li>
                <li class="timeline-inverted">
                  <div class="timeline-badge primary"><i class="fas fa-volume-up"></i></div>
                  <div class="timeline-panel">
                    <div class="timeline-heading">
                      <h4 class="timeline-title">Promover a comunicação</h4>
                    </div>
                    <div class="timeline-body">
                        <p>Promover a comunicação interna e com as partes interessadas, buscando a conscientização, envolvimento e o desenvolvimento dos colaboradores em atendimento as necessidades operacionais, normativas e legais.</p>
                     </div>
                  </div>
                </li>
                <li>
                    <div class="timeline-badge info"><i class="fas fa-balance-scale-left"></i></div>
                    <div class="timeline-panel">
                      <div class="timeline-heading">
                        <h4 class="timeline-title">Cumprir a Legislação</h4>
                      </div>
                      <div class="timeline-body">
                          <p>Cumprir a legislação regulamentar, requisitos éticos, morais e ambientais e de clientes relacionados a segurança de alimentos, saúde e segurança, meio ambiente e qualidade.</p>
                      </div>
                    </div>
                  </li>
                <li class="timeline-inverted">
                  <div class="timeline-badge primary"><i class="fas fa-hard-hat"></i></div>
                  <div class="timeline-panel">
                    <div class="timeline-heading">
                      <h4 class="timeline-title">Prover locais de trabalho seguros e saudáveis</h4>
                    </div>
                    <div class="timeline-body">
                        <p>Cumprir a legislação regulamentar, requisitos éticos, morais e ambientais e de clientes relacionados a segurança de alimentos, saúde e segurança, meio ambiente e qualidade.</p>
                    </div>
                  </div>
                </li>
                <li>
                    <div class="timeline-badge success"><i class="fab fa-pagelines"></i></div>
                    <div class="timeline-panel">
                      <div class="timeline-heading">
                        <h4 class="timeline-title">Preservar o Meio Ambiente</h4>
                      </div>
                      <div class="timeline-body">
                        <p>Preservar o Meio Ambiente, minimizando os impactos ambientais dos processos produtivos, prevenindo a poluição do ambiente, utilizando fontes renováveis de energia, respeitando a vida e fazendo uso racional dos recursos naturais.</p>
                    </div>
                    </div>
                  </li>
                <li class="timeline-inverted">
                  <div class="timeline-badge primary"><i class="fas fa-chart-line"></i></div>
                  <div class="timeline-panel">
                    <div class="timeline-heading">
                      <h4 class="timeline-title">Buscar a melhoria continua</h4>
                    </div>
                    <div class="timeline-body">
                        <p>Buscar a melhoria continua dos processos, sistemas de gestão e produtos, pro meio de um efetivo sistema de gerenciamento e da análise de desempenho, provendo recursos necessários de forma consciente e responsável.</p>
                    </div>
                  </div>
                </li>
            </ul>
        </div>
        <div class="p-3 mb-2 bg-primary text-white col-md-12 text-center">
            <span><i class="fas fa-5x fa-users"></i></span>
            <span class="float-right m-4 text-break">A <b>Usina Sonora</b> assume o compromisso de cumprir em todos os níveis da organização, a <b>Política do Sistema de Gestão Integrado</b>.</span>
        </div>
        <div class="col-md-12">&nbsp;</div>
    </div>
</div>
@endsection
