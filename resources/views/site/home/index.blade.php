@extends('layouts.masterpage')
@section('css')
    <link type="text/css" rel="stylesheet" href="{{ url('assets/css/news.css') }}" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
@endsection
@section('content')
<div class="container mt-5">
    <div class="row mt-5">
        <div class="col-md-12 col-xs-12">
            <h3 class="title-h3"><span  unselectable="on">Nossos Produtos</span></h3>
        </div>
        <div class="col-md-12 col-xs-12 mt-5">

            <div id="products" class="owl-carousel">
                <div class="text-center col-md-8 col-xs-8 p-3 btn btn-success" style="border-radius:0px !important">
                    <a class="text-decoration-none" href="{{ route('product','cana-de-acucar') }}">
                    <svg><use xlink:href="{{ url('assets/images/icon-product')}}/sugar-cane.svg#Capa_1"></use></svg>
                    <h3 class="text-white">Cana de Açúcar</h3>
                    </a>
                </div>
                <div class="text-center col-md-8 col-xs-8 p-3 btn btn-success" style="border-radius:0px !important">
                    <a class="text-decoration-none" href="{{ route('product','grao') }}">
                    <svg><use xlink:href="{{ url('assets/images/icon-product')}}/wheat.svg#Capa_1"></use></svg>
                    <h3 class="text-white">Grãos</h3>
                    </a>
                </div>
                <div class="text-center col-md-8 col-xs-8 p-3 btn btn-success" style="border-radius:0px !important">
                    <a class="text-decoration-none" href="{{ route('product','acucar') }}">
                    <svg><use xlink:href="{{ url('assets/images/icon-product')}}/sugar-cubes.svg#Capa_1"></use></svg>
                    <h3 class="text-white">Açúcar</h3>
                    </a>
                </div>
                <div class="text-center col-md-8 col-xs-8 p-3 btn btn-success" style="border-radius:0px !important">
                    <a class="text-decoration-none" href="{{ route('product','etanol') }}">
                    <svg><use xlink:href="{{ url('assets/images/icon-product')}}/etanol.svg#Capa_1"></use></svg>
                    <h3 class="text-white">Etanol</h3>
                    </a>
                </div>
                <div class="text-center col-md-8 col-xs-8 p-3 btn btn-success" style="border-radius:0px !important">
                    <a class="text-decoration-none" href="{{ route('product','energia-eletrica') }}">
                    <svg><use xlink:href="{{ url('assets/images/icon-product')}}/plug.svg#Capa_1"></use></svg>
                    <h3 class="text-white">Energia Elétrica</h3>
                    </a>
                </div>
            </div>
        </div>

    </div>
    <div class="row mt-5">
        <div class="col-md-12 col-xs-12">
            <h3 class="title-h3"><span  unselectable="on">Últimas Notícias</span></h3>
        </div>
        <div class="col-md-12 mt-5">
            <div id="news-slider8" class="owl-carousel">
                @foreach ($news as $new)
                <div class="post-slide8"  style="height: 440px !important">
                    <div class="post-img">
                        <img src="http://bestjquery.com/tutorial/news-slider/demo24/images/img-2.jpg" alt="">
                        <div class="post-date">
                            <span class="date">{{ \Carbon\Carbon::parse($new->date)->formatLocalized('%d') }}</span>
                            <span class="month">{{ \Carbon\Carbon::parse($new->date)->formatLocalized('%b') }}</span>
                        </div>
                    </div>
                    <div class="post-content">
                        <h3 class="post-title">
                            <a href="{{ route('news.detail',[$new->id,$new->tag]) }}">{{ $new->title }}</a>
                        </h3>
                        <a href="{{ route('news.detail',[$new->id,$new->tag]) }}" style="top:100% !important" class="read-more">Leia MAIS</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">&nbsp;</div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
    $("#products").owlCarousel({
        items : 3,
       itemsDesktop:[1199,3],
        itemsDesktopSmall:[980,2],
        itemsMobile : [600,1],
        autoPlay:true,
        nav: true,
    });
    $("#news-slider8").owlCarousel({
        items : 3,
        itemsDesktop:[1199,3],
        itemsDesktopSmall:[980,2],
        itemsMobile : [600,1],
        autoPlay:true
    });
</script>
@endsection
