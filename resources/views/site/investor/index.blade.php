@extends('layouts.masterpage')
@section('content')
<div class="col-md-12" style="background-color: #2F3861">
    <div class="row">
        <div class="col-md-3 offset-md-1 p-5 sub-header text-white"><h3>Investidores</h3></div>
    </div>
</div>
<div class="container m-5">
    <div class="row">
        <div class="col-md-8 offset-md-3">
            <h5 class="mb-5">Para obter os documentos você deverá preencher o formulário abaixo. Após o preenchimento, você receberá uma senha por email que permitirá fazer o download.</h5>
            <span class="mb-5"><b>* O E-mail é importante para o recebimento da senha. <br>
                     * Todos os campos são obrigatórios. </b></span>
        </div>
        <div class="col-md-8 offset-md-3 mt-3">
            <div class="card shadow border-0">
                <div class="card-header" style="background-color: transparent;border-bottom:none;border-top:0.38rem solid rgb(64, 56, 133)"><b>Novo Cadastro</b></div>
                <div class="card-body">
                    <form action="{{ route('investor') }}" method="post">
                        <div class="form-group">
                            <label for="name"><b>Nome <sup>(*)</sup></b></label>
                            <input type="text" name="name" class="form-control" required placeholder="Nome Completo" id="name">
                        </div>
                        <div class="form-group">
                            <label for="enterprise"><b>Empresa</b></label>
                            <input type="text" name="enterprise" class="form-control" placeholder="Empresa" id="enterprise">
                        </div>
                        <div class="form-group">
                            <label for="email"><b>E-mail <sup>(*)</sup></b></label>
                            <input type="text" name="email" class="form-control" required placeholder="E-mail" id="email">
                        </div>
                        <div class="form-group">
                            <label for="phone"><b>Telefone <sup>(*)</sup></b></label>
                            <input type="text" name="phone" class="form-control" required placeholder="(99) 9999-9999" id="phone">
                        </div>
                        <div class="form-group">
                            <label for="relationship"><b>Relacionamento</b></label>
                            <select class="form-control" name="relationship" id="relationship">
                                <option value="1" selected="selected">Acionista</option>
                                <option value="2">Agência de Rating</option>
                                <option value="3">Imprensa</option>
                                <option value="4">Instituição Financeira</option>
                                <option value="5">Investidor</option>
                                <option value="6">Outros</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Cadastrar" class="btn btn-primary btn-block">
                        </div>
                    </form>
                </div>
              </div>
        </div>
    </div>
</div>
@endsection
