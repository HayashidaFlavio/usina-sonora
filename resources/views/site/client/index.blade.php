@extends('layouts.masterpage')
@section('content')
<div class="col-md-12" style="background-color: #2F3861">
    <div class="row">
        <div class="col-md-3 offset-md-1 p-5 sub-header text-white"><h3>Clientes</h3></div>
    </div>
</div>
<div class="container p-5">
    <div class="row">
        <div class="col-md-12">
            <?php echo $data->content; ?>
        </div>
    </div>
</div>
@endsection
