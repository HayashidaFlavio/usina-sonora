@extends('adminlte::page')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content_header')
    <div class="row">
        <div class="col-11"><h1>Dashboard</h1></div>
        <div class="col-1 mt-1 text-right">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
        </ol>
        </div>
    </div>
@stop

@section('content')
<div class="row">
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-cyan"><i class="far fa-newspaper"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Notícias</span>
              <span class="info-box-number">{{ $total_news }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Acessos</span>
              <span class="info-box-number">{{ $total_users }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fas fa-chart-line" style="color:white"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Visitas</span>
              <span class="info-box-number">{{ $total_users }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
</div>
@stop
@section('footer')
<center>
Todo o direito reservado a <b>Usina Sonora MS - <?php echo date('Y'); ?></b>
</center>
@stop
