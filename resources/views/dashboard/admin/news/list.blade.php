@extends('adminlte::page')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content_header')
    <div class="row">
        <div class="col-9"><h1>Notícias</h1></div>
        <div class="col-3 mt-1 text-right">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Listar Notícias</li>
        </ol>
        </div>
    </div>
@stop

@section('content')

<div class="card">
              <div class="card-header text-right">
                <a href="{{ route('dashboard.admin.news.add') }}" class="btn btn-info"><i class="fas fa-plus-circle"></i> Adicionar</a>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Título</th>
                      <th>Data</th>
                      <th>Visualização</th>
                      <th>Status</th>
                      <th colspan="3" style="width:13%"><center>Ações</center></th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($datas as $data)
                      <tr>
                          <td>{{ $data->id }}</td>
                          <td>{{ $data->title }}</td>
                          <td>{{ \Carbon\Carbon::parse($data->date)->format('d/m/Y') }}</td>
                          <td>{{ $data->view }}</td>
                          <input type="hidden" id="hidden_status_{{ $data->id}}" value="{{ $data->status }}">
                          <td> @if($data->status == 1)<span id="status_{{ $data->id }}" onclick="updateStatus({{ $data->id }},'news')" class="badge badge-success">Ativo</span>@else<span onclick="updateStatus({{ $data->id }},'news')" id="status_{{ $data->id }}" class="badge badge-danger">Inativo</span>@endif</td>
                          <td>
                              <a href="{{ route( 'dashboard.admin.news.edit' ) }}/{{ $data->id }}/{{ $data->tag }}" id="button_edit_{{ $data->id }}" class="btn btn-info  @if($data->status == 0) disabled @endif" title="Editar Registro" alt="Editar Registro"><i class="fas fa-edit"></i></a>
                              <a href="#" class="btn btn-danger btn-delete @if($data->status == 0) disabled @endif" id="button_del_{{ $data->id }}" onclick="javascript:deleteRegister({{ $data->id }},'news')" title="Deletar Registro" alt="Deletar Registro"><i class="far fa-trash-alt"></i></a>
                          </td>
                      </tr>
                  @empty
                      <tr>
                            <td colspan="8"><center><h4>Não possui registro</h4></center></td>
                      </tr>
                  @endforelse
                  </tbody>
                </table>
              </div>
              @if ($datas->count() > 0)
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <div class="d-flex justify-content-center">
                        {{ $datas->render() }}
                 </div>
              </div>
              @endif
            </div>

@stop
@section('footer')
<center>
Todo o direito reservado a <b>Usina Sonora MS - <?php echo date('Y'); ?></b>
</center></center>
@stop
@section('js')

<script type="text/javascript" src="{{ asset('vendor/delete/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/status/status.js') }}"></script>

@stop
