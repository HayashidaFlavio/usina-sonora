@extends('adminlte::page')

@section('content_header')
    <div class="row">
        <div class="col-8"><h1>Produto</h1></div>
        <div class="col-4 mt-1 text-right">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.product.list') }}">Lista de Produtos</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $button_action }} Produto</li>
        </ol>
        </div>
    </div>
@stop
@section('content')
<div class="card card-primary rounded-0 border-top border-primary" style="border-top-width: medium !important;">
    <form  method="POST" action="{{ route( $form_action ) }}" enctype="multipart/form-data">
    @csrf
    @if (!empty($data->id))
        <input type="hidden" name="id" value="{{ $data->id }}">
    @endif
        <div class="card-body">
            <div class="form-group @error('title')  has-error @enderror">
                <label for="title">Título</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="@if (!empty($data->title)) {{ $data->title }}  @endif" placeholder="Título da notícia">

                @error('title')
                    <span class="help-block is-invalid" style="color:red"  role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="tag">Tag</label>
                <input type="text" class="form-control" id="tag" name="tag" value="@if (!empty($data->tag)) {{ $data->tag }} @endif" placeholder="Tag" readonly>
            </div>
            <div class="form-group">
                <label for="image">Imagem</label>
                <input type="hidden" name="imageOld" value="@if (!empty($data->image)) {{ $data->image }} @endif">
                <input type="file" class="form-control" accept="image/*" id="image" name="image" value="@if (!empty($data->image)) {{ $data->image }} @endif" placeholder="Imagem">
            </div>
            <div class="form-group @error('content')  has-error @enderror">
                <label for="content">Conteúdo</label>
                <textarea name="content" id="content" class="form-control @error('content')  is-invalid @enderror" cols="30" rows="10">@if (!empty($data->content)) {{ $data->content }} @endif</textarea>
                @error('content')
                    <span class="help-block is-invalid" style="color:red" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <p><label for="status">Status</label></p>
                <input type="checkbox" name="status" id="status" data-style="slow" @if(!empty($data->status)) @if($data->status == 1) checked @endif @endif data-toggle="toggle" data-on="Ativo" data-off="Inativo" data-width="100" data-onstyle="success" data-offstyle="danger" data-height="50">
           </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-block btn-primary"><i class="{{ $button_icon }}"></i> {{ $button_action }}</button>
        </div>
    </form>
</div>
@stop
@section('footer')
<center>
Todo o direito reservado a <b>Usina Sonora MS - <?php echo date('Y'); ?></b>
</center>
@stop
@section('css')
<style>
  .slow  .toggle-group { transition: left 0.7s; -webkit-transition: left 0.7s; }
</style>
@stop
@section('js')
<script type="text/javascript" src="{{ asset('vendor/accents/remove-accents.js') }}"></script>
<script>
$(document).ready(function(){
  CKEDITOR.replace( 'content' );
});
$("#title").change(function(){
    var new_title = retira_acentos($('#title').val()).toLowerCase().replace(/ /g, "-")
    $('#tag').val(new_title);
  });
</script>
    @if (session('message'))
<script>
    Swal.fire({
        icon:  '{{ session('icon') }}',
        title: '{{ session('message') }}',
    })
</script>
    @endif
@stop
