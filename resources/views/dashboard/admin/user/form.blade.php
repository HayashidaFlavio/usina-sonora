@extends('adminlte::page')

@section('content_header')
    <div class="row">
        <div class="col-8"><h1>Usuário</h1></div>
        <div class="col-4 mt-1 text-right">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.user.list') }}">Lista de Usuários</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $button_action }} Usuário</li>
        </ol>
        </div>
    </div>
@stop

@section('content')
<div class="card card-primary rounded-0 border-top border-primary" style="border-top-width: medium !important;">
    <form  method="POST" action="{{ route( $form_action ) }}" enctype="multipart/form-data">
    @csrf
    @if (!empty($data->id))
        <input type="hidden" name="id" value="{{ $data->id }}">
    @endif
        <div class="card-body">
            <div class="form-group @error('title')  has-error @enderror">
                <label for="title">Nome</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="@if (!empty($data->name)) {{ $data->name }}  @endif" placeholder="Nome completo">
                @error('name')
                    <span class="help-block is-invalid" style="color:red"  role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group  @error('email')  has-error @enderror">
                <label for="email">E-mail</label>
                <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="@if (!empty($data->email)) {{ $data->email }} @endif" placeholder="E-mail">
                @error('email')
                <span class="help-block is-invalid" style="color:red"  role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            </div>
            <div class="form-group  @error('password')  has-error @enderror">
                <label for="date">Senha</label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" value="@if (!empty($data->password)){{ $data->password }}@endif">
                @error('password')
                <span class="help-block is-invalid" style="color:red"  role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            </div>
            <div class="form-group">
                <label for="date">Status</label>
                <input type="checkbox" name="status" data-style="slow" @if(!empty($data->status)) @if($data->status == 1) checked @endif @endif data-toggle="toggle" data-on="Ativo" data-off="Inativo" data-width="100" data-onstyle="success" data-offstyle="danger" data-height="50">
           </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-block btn-primary"><i class="{{ $button_icon }}"></i> {{ $button_action }}</button>
        </div>
    </form>
</div>
@stop
@section('footer')
<center>
Todo o direito reservado a <b>Usina Sonora MS - <?php echo date('Y'); ?></b>
</center>
@stop
@section('css')
<style>
  .slow  .toggle-group { transition: left 0.7s; -webkit-transition: left 0.7s; }
</style>
@stop
@section('js')
<script type="text/javascript" src="{{ asset('vendor/accents/remove-accents.js') }}"></script>
<script>
$(document).ready(function(){
    $("#date").mask("00/00/0000");
  CKEDITOR.replace( 'content' );

});
</script>
    @if (session('message'))
<script>
    Swal.fire({
        icon:  '{{ session('icon') }}',
        title: '{{ session('message') }}',
    })
</script>
    @endif
@stop
