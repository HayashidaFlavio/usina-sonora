@extends('adminlte::page')

@section('content_header')
    <div class="row">
        <div class="col-7"><h1>Investidor</h1></div>
        <div class="col-5 mt-1 text-right">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.investor.list') }}">Lista de Investidores</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $button_action }} Investidor</li>
        </ol>
        </div>
    </div>
@stop

@section('content')
<div class="card card-primary rounded-0 border-top border-primary" style="border-top-width: medium !important;">
    <form  method="POST" action="{{ route( $form_action ) }}" enctype="multipart/form-data">
    @csrf
    @if (!empty($data->id))
        <input type="hidden" name="id" value="{{ $data->id }}">
    @endif
        <div class="card-body">
            <div class="form-group @error('title')  has-error @enderror">
                <label for="title">Nome</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="@if (!empty($data->name)) {{ $data->name }}  @endif" placeholder="Nome completo">
                @error('name')
                    <span class="help-block is-invalid" style="color:red"  role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="text" class="form-control" id="email" name="email" value="@if (!empty($data->email)) {{ $data->email }} @endif" placeholder="E-mail">
            </div>
            <div class="form-group">
                <label for="password">Senha</label>
                @if (!empty($data->password))
                <input type="password" class="form-control" id="password" name="password" value="@if (!empty($data->password)){{ $data->password }}@endif">
                @else
                <div class="row">
                    <div class="col-md-10">
                    <input type="text" class="form-control" id="password" name="password" value="@if (!empty($data->password)){{ $data->password }}@endif">
                    </div>
                    <div class="col-md-2"><a href="#" class="btn btn-info" onclick="getPassword()"><i class="fas fa-unlock-alt"></i> Gerar Senha</a></div>
                </div>
                 @endif
            </div>
            <div class="form-group">
                <label for="date">Status</label>
                <input type="checkbox" name="status" data-style="slow" @if(!empty($data->status)) @if($data->status == 1) checked @endif @endif data-toggle="toggle" data-on="Ativo" data-off="Inativo" data-width="100" data-onstyle="success" data-offstyle="danger" data-height="50">
           </div>

           <div class="form-group">
            <label for="phone">Telefone</label>
            <input type="text" class="form-control" id="phone" name="phone" value="@if (!empty($data->phone)) {{ $data->phone }} @endif" placeholder="Telefone">
        </div>
        <div class="form-group">
            <label for="enterprise">Empresa</label>
            <input type="text" class="form-control" id="enterprise" name="enterprise" value="@if (!empty($data->enterprise)) {{ $data->enterprise }} @endif" placeholder="Empresa">
        </div>
        <div class="form-group">
            <label for="relationship">Relacionamento</label>
            <select class="form-control" name="relationship" id="relationship">
                <option value="">Selecione</option>
                <option value="1" @if(!empty($data->relationship)) @if($data->relationship == 1) selected @endif @endif>Acionista</option>
                <option value="2" @if(!empty($data->relationship)) @if($data->relationship == 2) selected @endif @endif>Agencia de Rating</option>
                <option value="3" @if(!empty($data->relationship)) @if($data->relationship == 3) selected @endif @endif>Imprensa</option>
                <option value="4" @if(!empty($data->relationship)) @if($data->relationship == 4) selected @endif @endif>Instituição Financeira</option>
                <option value="5" @if(!empty($data->relationship)) @if($data->relationship == 5) selected @endif @endif>Investidor</option>
                <option value="6" @if(!empty($data->relationship)) @if($data->relationship == 6) selected @endif @endif>Outros</option>
            </select>
         </div>
        </div>


        <div class="card-footer">
            <button type="submit" class="btn btn-block btn-primary"><i class="{{ $button_icon }}"></i> {{ $button_action }}</button>
        </div>
    </form>
</div>
@stop
@section('css')
<style>
  .slow  .toggle-group { transition: left 0.7s; -webkit-transition: left 0.7s; }
</style>
@stop
@section('footer')
<center>
Todo o direito reservado a <b>Usina Sonora MS - <?php echo date('Y'); ?></b>
</center>
@stop
@section('js')
<script type="text/javascript" src="{{ asset('vendor/accents/remove-accents.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/generatepassword/password.js') }}"></script>
<script>
$(document).ready(function(){
    $("#date").mask("00/00/0000");
  CKEDITOR.replace( 'content' );

});
</script>
    @if (session('message'))
<script>
    Swal.fire({
        icon:  '{{ session('icon') }}',
        title: '{{ session('message') }}',
    })
</script>
    @endif
@stop
