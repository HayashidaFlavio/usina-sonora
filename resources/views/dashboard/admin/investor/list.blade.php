@extends('adminlte::page')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content_header')
    <div class="row">
        <div class="col-9"><h1>Investidores</h1></div>
        <div class="col-3 mt-1 text-right">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Listar Investidores</li>
        </ol>
        </div>
    </div>
@stop

@section('content')

<div class="card">
              <div class="card-header text-right d-none">
                <a href="{{ route('dashboard.admin.investor.add') }}" class="btn btn-info sr-only"><i class="fas fa-plus-circle"></i> Adicionar</a>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Nome</th>
                      <th>E-mail</th>
                      <th>Telefone</th>
                      <th>Status</th>
                      <th colspan="3"><center>Ações</center></th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($datas as $data)
                      <tr>
                          <td>{{ $data->id }}</td>
                          <td>{{ $data->name }}</td>
                          <td>{{ $data->email }}</td>
                          <td>{{ $data->phone }}</td>
                          <input type="hidden" id="hidden_status_{{ $data->id}}" value="{{ $data->status }}">
                          <td> @if($data->status == 1)<span id="status_{{ $data->id }}" onclick="updateStatus({{ $data->id }},'investor')" class="badge badge-success">Ativo</span>@else<span onclick="updateStatus({{ $data->id }},'investor')" id="status_{{ $data->id }}" class="badge badge-danger">Inativo</span>@endif</td>
                          <td>
                              <a href="{{ route( 'dashboard.admin.investor.edit' ) }}/{{ $data->id }}" class="btn btn-info @if($data->status == 0) disabled @endif" title="Editar Registro"  id="button_edit_{{ $data->id }}" alt="Editar Registro"><i class="fas fa-edit"></i></a>
                              <a href="#" class="btn btn-danger btn-delete @if($data->status == 0) disabled @endif" id="button_del_{{ $data->id }}" onclick="javascript:deleteRegister({{ $data->id }},'user')" title="Deletar Registro" alt="Deletar Registro"><i class="far fa-trash-alt"></i></a>
                              <a href="#" class="btn btn-info @if($data->status == 0) disabled @endif" id="button_view_{{ $data->id }}" title="Visualizar Registro" alt="Visualizar Registro" onclick="openModal({{ $data->id }},'{{ $data->name }}','{{ $data->email }}','{{ $data->phone }}')"><i class="fas fa-eye"></i></a>
                            </td>
                      </tr>
                    @empty
                      <tr>
                            <td colspan="8"><center><h4>Não possui registro</h4></center></td>
                      </tr>
                        @endforelse
                  </tbody>
                </table>
              </div>
              <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-xl">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="title-name"></h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <form method="POST" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <input type="hidden" name="id" id="id">
                                <div class="col-md-12"><b>Nome</b></div>
                            </div>
                            <div class="row pt-1">
                                <div class="col-md-9"><input type="text" id="name" name="name" class="form-control" readonly></div>
                            </div>
                            <div class="row pt-1">
                                <div class="col-md-6"><b>E-mail</b></div>
                                <div class="col-md-6"><b>Telefone</b></div>
                            </div>
                            <div class="row pt-1">
                                <div class="col-md-6"><input type="text" name="email" id="email" class="form-control" readonly></div>
                                <div class="col-md-6"><input type="text" id="phone" class="form-control" readonly></div>
                            </div>
                            <div class="row pt-1">
                                <div class="col-md-12"><b>Gerar Senha</b></div>
                            </div>
                            <div class="row pt-1">
                                <div class="col-md-4">
                                <input type="text" class="form-control" id="password" name="password">
                                </div>
                                <div class="col-md-3"><a href="#" class="btn btn-info" onclick="getPassword()"><i class="fas fa-unlock-alt"></i> Gerar Senha</a></div>
                            </div>
                            <div class="row pt-1">
                                <div class="col-md-12"><b>Enviar formulário do investidor</b></div>
                            </div>
                            <div class="row pt-1">
                                <div class="col-md-12">
                                    <div class="custom-file">
                                    <input type="file" name="file" id="file" class="custom-file-input">
                                    <label class="custom-file-label" for="customFile">Selecione o Arquivo</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="passwordGenerate" name="password-generate"><i class="far fa-envelope"></i> Enviar E-mail com a nova senha</button>
                      <button  type="button" class="btn btn-primary" id="fileUpload" name="file-upload"><i class="far fa-envelope"></i> Enviar E-mail com o formulario de investidor</button>
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                  </div>
                </form>
                </div>
              </div>

              @if ($datas->count() > 0)
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <div class="d-flex justify-content-center">
                        {{ $datas->render() }}
                 </div>
              </div>
              @endif
            </div>



@stop
@section('footer')
<center>
Todo o direito reservado a <b>Usina Sonora MS - <?php echo date('Y'); ?></b>
</center>
@stop
@section('js')

<script type="text/javascript" src="{{ asset('vendor/delete/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/openmodal/modal.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/status/status.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/uploadAjax/upload.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/generatePassword/password.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/generatepassword/password.js') }}"></script>

@stop
