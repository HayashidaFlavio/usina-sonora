@extends('adminlte::page')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content_header')
    <div class="row">
        <div class="col-8"><h1>E-mail departamentos</h1></div>
        <div class="col-4 mt-1 text-right">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Listar E-mail departamentos</li>
        </ol>
        </div>
    </div>
@stop
@section('content')
<div class="card">
    <div class="card-header text-right">
        <a href="{{ route('dashboard.admin.parameter.building.add') }}" class="btn btn-info"><i class="fas fa-plus-circle"></i> Adicionar</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Departamento</th>
                    <th>Responsável</th>
                    <th>Status</th>
                    <th colspan="3" style="width:13%"><center>Ações</center></th>
                </tr>
            </thead>
            <tbody>
            @forelse ($datas as $data)
                <tr>
                    <td>{{ $data->id }}</td>
                    <td>{{ $data->departament }}</td>
                    <td>{{ $data->responsible }}</td>
                    <td> @if($data->status == 1)<span id="status" class="badge badge-success">Ativo</span>@else <span id="status" class="badge badge-danger">Inativo</span> @endif</td>
                    <td>
                        <a href="{{ route( 'dashboard.admin.parameter.building.edit',$data->id ) }}" class="btn btn-info"><i class="fas fa-edit" title="Editar Registro" alt="Editar Registro"></i></a>
                        <a href="#" class="btn btn-danger btn-delete @if($data->status == 0) disabled @endif" id="button_{{ $data->id }}" onclick="javascript:deleteRegister({{ $data->id }},'departament')" title="Deletar Registro" alt="Deletar Registro"><i class="far fa-trash-alt"></i></a>
                    </td>
                </tr>
            @empty
                 <tr>
                    <td colspan="8"><center><h4>Não possui registro</h4></center></td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    @if ($datas->count() > 10)
    <!-- /.card-body -->
    <div class="card-footer clearfix">
        <div class="d-flex justify-content-center">
            {{ $datas->links() }}
        </div>
    </div>
    @endif
</div>
@stop
@section('footer')
    <center> Todo o direito reservado a <b>Usina Sonora MS - <?php echo date('Y'); ?></b> </center>
@stop
@section('js')
<script type="text/javascript" src="{{ asset('vendor/delete/app.js') }}"></script>
@stop
