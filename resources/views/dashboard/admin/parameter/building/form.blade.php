@extends('adminlte::page')
@section('css')
    <link rel="stylesheet" href="{{ url('assets/css') }}/switch.css">
@endsection
@section('content_header')

<div class="container-fluid">
    <div class="row mb-2">
    <div class="col-sm-6"><h1>E-mail Departamentos</h1></div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $button_action }} E-mail Departamentos</li>
            </ol>
        </div>
    </div>
</div>
@stop

@section('content')
<div class="card card-primary rounded-0 pb-5 border-top border-primary" style="border-top-width: medium !important;">
    <div class="card-body">
        <form method="POST" action="{{ route( $form_action ) }}">
            @csrf
            <input type="hidden" name="id" value="@if(!empty($data->id)) {{$data->id }} @endif">
            <div class="form-group">
                <label for="responsible"><b>Responsável</b></label>
                <input type="text" name="responsible" id="responsible" value="@if(!empty($data->responsible)) {{ $data->responsible }} @endif" placeholder="Nome do Responsável" class="form-control">
            </div>
            <div class="form-group">
                <label for="departament"><b>Departamento</b></label>
                <input type="text" name="departament" id="departament" value="@if(!empty($data->departament)) {{ $data->departament }} @endif" placeholder="Nome do Departamento" class="form-control phone">
            </div>
            <div class="form-group">
                <label for="email"><b>E-mail</b></label>
                <input type="text" name="email" id="email" value="@if(!empty($data->email)) {{ $data->email }} @endif" placeholder="E-mail do departamento" class="form-control">
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <div class="material-switch">
                    <input id="someSwitchOptionPrimary" @if(!empty($data->email)) @if($data->id == 1) checked @endif @endif name="status" type="checkbox"/>
                    <label for="someSwitchOptionPrimary" class="btn-primary"></label>
                </div>
            </div>
        </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-block btn-primary"><i class="{{ $button_icon }}"></i> {{ $button_action }}</button>
    </div>
</form>
</div>
@stop
@section('footer')
<center>
Todo o direito reservado a <b>Usina Sonora MS - <?php echo date('Y'); ?></b>
</center>
@stop
@section('css')
<style>
  .slow  .toggle-group { transition: left 0.7s; -webkit-transition: left 0.7s; }
</style>
@stop
@section('js')
    @if (session('message'))
<script>
    Swal.fire({
        icon:  '{{ session('icon') }}',
        title: '{{ session('message') }}',
    })
</script>
    @endif
@stop

