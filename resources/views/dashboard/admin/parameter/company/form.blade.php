@extends('adminlte::page')

@section('content_header')

<div class="container-fluid">
    <div class="row mb-2">
    <div class="col-sm-6"><h1>Parâmetros Empresa</h1></div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Editar Parâmetro Empresa</li>
            </ol>
        </div>
    </div>
</div>
@stop

@section('content')
<div class="card card-primary rounded-0 border-top border-primary" style="border-top-width: medium !important;">
    <div class="card-body">
        <form method="POST" action="{{ route( $form_action ) }}">
            @csrf
            <input type="hidden" name="id" value="{{ $data->id }}">
            <input type="hidden" name="type" value="{{ $type }}">
            <div class="form-group">
                <label for="name"><b>Nome</b></label>
                <input type="text" name="name" id="name" value="{{ $data->name }}" class="form-control">
            </div>
            <div class="form-group">
                <label for="phone"><b>Telefone</b></label>
                <input type="text" name="phone" id="phone" value="{{ $data->phone }}" class="form-control phone">
            </div>
            <div class="form-group">
                <label for="fax"><b>Fax</b></label>
                <input type="text" name="fax" id="fax" value="{{ $data->fax }}" class="form-control phone">
            </div>
            <div class="form-group">
                <label for="whatsapp"><b>Whatsapp</b></label>
                <input type="text" name="whatsapp" id="whatsapp" value="{{ $data->whatsapp }}" class="form-control cellphone">
            </div>
            <div class="form-group">
                <label for="address"><b>Endereço</b></label>
                <input type="text" name="address" id="address" value="{{ $data->address }}" class="form-control">
            </div>
            <div class="form-group">
                <label for="facebook"><b>Facebook</b></label>
                <input type="text" name="facebook" id="facebook" value="{{ $data->facebook }}" class="form-control">
            </div>
            <div class="form-group">
                <label for="linkedin"><b>Linkedin</b></label>
                <input type="text" name="linkedin" id="linkedin" value="{{ $data->linkedin }}" class="form-control">
            </div>
            <div class="form-group">
                <label for="instagram"><b>Instagram</b></label>
                <input type="text" name="instagram" id="instagram" value="{{ $data->instagram }}" class="form-control">
            </div>
            <div class="form-group">
                <label for="maps"><b>Maps</b></label>
                <input type="text" name="maps" id="maps" value="{{ $data->maps }}" class="form-control">
            </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-block btn-primary"><i class="{{ $button_icon }}"></i>{{ $button_action }}</button>
    </div>
</form>
</div>
@stop
@section('footer')
<center>
Todo o direito reservado a <b>Usina Sonora MS - <?php echo date('Y'); ?></b>
</center>
@stop
@section('css')
<style>
  .slow  .toggle-group { transition: left 0.7s; -webkit-transition: left 0.7s; }
</style>
@stop
@section('js')
<script>
    $('.phone').mask('+00 (00) 0000-0000');
    $('.cellphone').mask('+00 (00) 00000-0009');
</script>
    @if (session('message'))
<script>
    Swal.fire({
        icon:  '{{ session('icon') }}',
        title: '{{ session('message') }}',
    })
</script>
    @endif
@stop

