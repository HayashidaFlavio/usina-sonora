@extends('adminlte::page')

@section('content_header')

<div class="container-fluid">
    <div class="row mb-2">
    <div class="col-sm-6"><h1>Parâmetros E-mail</h1></div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Editar Parâmetro E-mail</li>
            </ol>
        </div>
    </div>
</div>
@stop

@section('content')
<div class="card card-primary rounded-0 border-top border-primary" style="border-top-width: medium !important;">
    <div class="card-body">
        <form method="POST" action="{{ route( $form_action ) }}">
            @csrf
            <input type="hidden" name="id" value="{{ $data->id }}">
            <input type="hidden" name="type" value="{{ $type }}">
            <div class="form-group">
                <label for="email"><b>Email para Investidor</b></label>
                <textarea name="email_investor" id="email" class="form-control" cols="30" rows="10"> {{ $data->content_email_investor }} </textarea>
            </div>
            <div class="form-group">
                <label for="email1"><b>Email Contato</b></label>
                <textarea name="email_contact" id="email1" class="form-control" cols="30" rows="10"> {{ $data->content_email_contact_answer }} </textarea>
            </div>
            <div class="form-group">
                <label for="email1"><b>Email para Investidor da senha</b></label>
                <textarea name="email_password_generate" id="email2" class="form-control" cols="30" rows="10"> {{ $data->content_email_investor_password_generate }} </textarea>
            </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-block btn-primary"><i class="{{ $button_icon }}"></i>{{ $button_action }}</button>
    </div>
</form>
</div>
@stop
@section('footer')
<center>
Todo o direito reservado a <b>Usina Sonora MS - <?php echo date('Y'); ?></b>
</center>
@stop
@section('css')
<style>
  .slow  .toggle-group { transition: left 0.7s; -webkit-transition: left 0.7s; }
</style>
@stop
@section('js')
<script>
CKEDITOR.replace( 'email' );
CKEDITOR.replace( 'email1' );
CKEDITOR.replace( 'email2' );
</script>
    @if (session('message'))
<script>
    Swal.fire({
        icon:  '{{ session('icon') }}',
        title: '{{ session('message') }}',
    })
</script>
    @endif
@stop

