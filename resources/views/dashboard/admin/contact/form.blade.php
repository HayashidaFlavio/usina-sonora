@extends('adminlte::page')

@section('content_header')
    <div class="row">
        <div class="col-9"><h1>Contato</h1></div>
        <div class="col-3 mt-1 text-right">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $button_action }} Contato</li>
        </ol>
        </div>
    </div>
@stop

@section('content')
<div class="card card-primary rounded-0 border-top border-primary" style="border-top-width: medium !important;">
    <form  method="POST" action="{{ route( $form_action ) }}">
    @csrf
    @if (!empty($data->id))
        <input type="hidden" name="id" value="{{ $data->id }}">
    @endif
        <div class="card-body">
            <div class="form-group @error('title')  has-error @enderror">
                <label for="title">Nome</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="@if (!empty($data->name)) {{ $data->name }}  @endif" placeholder="Nome completo">
                @error('name')
                    <span class="help-block is-invalid" style="color:red"  role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="text" class="form-control" id="email" name="email" value="@if (!empty($data->email)) {{ $data->email }} @endif" placeholder="E-mail">
            </div>
            <div class="form-group">
                <label for="date">Data</label>
                <input type="text" class="form-control" id="date" name="date" value="@if (!empty($data->created_at)){{ \Carbon\Carbon::parse($data->created_at)->format('d/m/Y') }}@endif">
            </div>
            <div class="form-group @error('content')  has-error @enderror">
                <label for="content">Conteúdo</label>
                <textarea name="content" id="content" class="form-control @error('content')  is-invalid @enderror" cols="30" rows="10">@if (!empty($data->content)) {{ $data->content }} @endif</textarea>
                @error('content')
                    <span class="help-block is-invalid" style="color:red" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-block btn-primary"><i class="{{ $button_icon }}"></i> {{ $button_action }}</button>
        </div>
    </form>
</div>
@stop
@section('footer')
<center>
Todo o direito reservado a <b>Usina Sonora MS - <?php echo date('Y'); ?></b>
</center></center>
@stop
@section('js')
<script type="text/javascript" src="{{ asset('vendor/accents/remove-accents.js') }}"></script>
<script>
$(document).ready(function(){
    $("#date").mask("00/00/0000");
  CKEDITOR.replace( 'content' );

});
$("#title").change(function(){
    var new_title = retira_acentos($('#title').val()).toLowerCase().replace(/ /g, "-")
    $('#tag').val(new_title);
  });
</script>
    @if (session('message'))
<script>
    Swal.fire({
        icon:  '{{ session('icon') }}',
        title: '{{ session('message') }}',
    })
</script>
    @endif
@stop
