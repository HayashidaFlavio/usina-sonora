@extends('adminlte::page')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content_header')
    <div class="row">
        <div class="col-9"><h1>Contatos</h1></div>
        <div class="col-3 mt-1 text-right">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Listar Contatos</li>
        </ol>
        </div>
    </div>
@stop

@section('content')

<div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Nome</th>
                      <th>E-mail</th>
                      <th>Data</th>
                      <th>Status</th>
                      <th colspan="3"><center>Ações</center></th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($datas as $data)
                      <tr>
                          <td>{{ $data->id }}</td>
                          <td>{{ $data->name }}</td>
                          <td>{{ $data->email }}</td>
                          <td>{{ \Carbon\Carbon::parse($data->created_at)->format('d/m/Y') }}</td>
                          <td> @if($data->status == 1)<span id="status_{{ $data->id }}" class="badge badge-success">Ativo</span>@else<span class="badge badge-danger">Inativo</span>@endif</td>
                          <td>
                              <a href="{{ route( 'dashboard.admin.contact.edit' ) }}/{{ $data->id }}/{{ $data->tag }}" class="btn btn-info" title="Editar Registro" alt="Editar Registro"><i class="fas fa-edit"></i></a>
                              <a href="#" class="btn btn-danger btn-delete @if($data->status == 0) disabled @endif" id="button_{{ $data->id }}" onclick="javascript:deleteRegister({{ $data->id }},'contact')" title="Deletar Registro" alt="Deletar Registro"><i class="far fa-trash-alt"></i></a>
                          </td>
                      </tr>
                  @empty
                      <tr>
                            <td colspan="8"><center><h4>Não possui registro</h4></center></td>
                      </tr>
                  @endforelse
                  </tbody>
                </table>
              </div>
              @if ($datas->count() > 0)
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <div class="d-flex justify-content-center">
                        {{ $datas->render() }}
                 </div>
              </div>
              @endif
            </div>

@stop
@section('footer')
<center>
Todo o direito reservado a <b>Usina Sonora MS - <?php echo date('Y'); ?></b>
</center>
@stop
@section('js')

<script type="text/javascript" src="{{ asset('vendor/delete/app.js') }}"></script>

@stop
