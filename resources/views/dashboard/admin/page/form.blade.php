@extends('adminlte::page')

@section('content_header')

<div class="container-fluid">
    <div class="row mb-2">
    <div class="col-sm-6"><h1>Página</h1></div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Editar Página</li>
            </ol>
        </div>
    </div>
</div>
@stop

@section('content')
<div class="card card-primary rounded-0 border-top border-primary" style="border-top-width: medium !important;">
    <form  method="POST" action="{{ route('dashboard.admin.page.edit') }}">
    @csrf
        <input type="hidden" name="id" value="{{ $data->id }}">

    @if (session('page'))
        <input type="hidden" name="page" value="{{ $page }}">
    @endif
        <div class="card-body">
            <div class="form-group">
                <label for="name">Nome</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $data->name }}" placeholder="Nome da Página">
            </div>
            <div class="form-group">
                <label for="tag">Tag</label>
                <input type="text" class="form-control" id="tag" name="tag" value="{{ $data->tag }}" placeholder="Tag" readonly>
            </div>
            <div class="form-group">
                <label for="image">Imagem</label>
                <input type="hidden" name="imageOld" value="@if (!empty($data->image)) {{ $data->image }} @endif">
                <input type="file" class="form-control" accept="image/*" id="image" name="image" value="@if (!empty($data->image)) {{ $data->image }} @endif" placeholder="Imagem">
            </div>
            <div class="form-group">
                <label for="content">Conteúdo</label>
                <textarea name="content" id="content" class="form-control" cols="30" rows="10">{{ $data->content }}</textarea>
            </div>
            <div class="form-group">
                <p><label for="status">Status</label></p>
                <input type="checkbox" name="status" id="status" data-style="slow" @if(!empty($data->status)) @if($data->status == 1) checked @endif @endif data-toggle="toggle" data-on="Ativo" data-off="Inativo" data-width="100" data-onstyle="success" data-offstyle="danger" data-height="50">
           </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-block btn-primary"><i class="{{ $button_icon }}"></i>{{ $button_action }}</button>
        </div>
    </form>
</div>
@stop
@section('footer')
<center>
Todo o direito reservado a <b>Usina Sonora MS - <?php echo date('Y'); ?></b>
</center></center>
@stop
@section('css')
<style>
  .slow  .toggle-group { transition: left 0.7s; -webkit-transition: left 0.7s; }
</style>
@stop
@section('js')
<script>
   CKEDITOR.replace( 'content' );
</script>
    @if (session('message'))
<script>
    Swal.fire({
        icon:  '{{ session('icon') }}',
        title: '{{ session('message') }}',
    })
</script>
    @endif
@stop

