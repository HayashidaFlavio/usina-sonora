@extends('adminlte::page')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content_header')
    <div class="row">
        <div class="col-9"><h1>Contatos</h1></div>
        <div class="col-3 mt-1 text-right">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Listar Contatos</li>
        </ol>
        </div>
    </div>
@stop

@section('content')

<div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Data</th>
                      <th>Arquivo</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($datas as $data)
                      <tr>
                          <td>{{ $data->id }}</td>
                          <td>{{ \Carbon\Carbon::parse($data->reference)->format('d/m/Y') }}</td>
                          <td><a target="_blank" href="{{ url('storage/documents/investor/'.$data->user_id,$data->document)}}">Relatório de investimento {{ \Carbon\Carbon::parse($data->reference)->locale('pt_BR')->format('F') }}</a></td>
                        </tr>
                  @empty
                      <tr>
                            <td colspan="8"><center><h4>Não possui registro</h4></center></td>
                      </tr>
                  @endforelse
                  </tbody>
                </table>
              </div>
              @if ($datas->count() > 10)
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <div class="d-flex justify-content-center">
                        {{ $datas->render() }}
                 </div>
              </div>
              @endif
            </div>

@stop
@section('footer')
<center>
Todo o direito reservado a <b>Usina Sonora MS - <?php echo date('Y'); ?></b>
</center>
@stop
@section('js')

<script type="text/javascript" src="{{ asset('vendor/delete/app.js') }}"></script>

@stop
