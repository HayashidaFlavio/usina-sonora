@extends('adminlte::page')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content_header')
    <div class="row">
        <div class="col-9"><h1>Dashboard</h1></div>
        <div class="col-3 mt-1 text-right">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.admin.home') }}"><i class="fas fa-home"></i> Home</a></li>
        </ol>
        </div>
    </div>
@stop

@section('content')


@stop

@section('js')


@stop
