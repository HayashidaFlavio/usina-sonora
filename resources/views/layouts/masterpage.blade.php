    <!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>{{ config('app.name', 'Laravel') }}</title>
            <link type="text/css" rel="stylesheet" href="{{ url('assets/css/style.css') }}" />
            <link type="text/css" rel="stylesheet" href="{{ url('assets/css/carousel.css') }}" />
            <link type="text/css" rel="stylesheet" href="{{ url('assets/css/animate.css') }}" />
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
            <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" integrity="sha512-RXf+QSDCUQs5uwRKaDoXt55jygZZm2V++WUZduaU/Ui/9EGp3f/2KZVahFZBKGH0s774sd3HmrhUy+SgOFQLVQ==" crossorigin="anonymous"></script>
            <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
            @yield('css')
        </head>
        <body>
            <header>
                <nav id="MainMenu" class="navbar fixed-top navbar-expand-lg navbar-light bg-light shadow">
                    <a class="navbar-brand ml-5" id="logo1" href="{{ route('home') }}"><img src="{{ url('vendor/adminlte/dist/img/') }}/usinasonora-full-3.png" alt=""></a>
                    <a class="navbar-brand ml-5 sr-only" id="logo2" href="{{ route('home') }}"><img src="{{ url('vendor/adminlte/dist/img/') }}/usinasonora-full-2.png" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarText">
                        <ul class="navbar-nav mr-auto text-right">
                            <li class="nav-item @if(Request::route()->getName() == 'home') active font-weight-bolder @endif">
                                <a class="nav-link" href="{{ route('home') }}"><i class="fas fa-1x fa-home"></i></a>
                            </li>
                            <li class="nav-item dropdown @if(Request::route()->getName() == 'factory') active font-weight-bolder @endif">
                                <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Usina
                                </a>
                                <div class="dropdown-menu shadow-invert" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('factory','historico') }}">Histórico</a>
                                <a class="dropdown-item" href="{{ route('factory','grupo-economico') }}">Grupo Econômico</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown  @if(Request::route()->getName() == 'product') active font-weight-bolder @endif">
                                <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Produtos
                                </a>
                                <div class="dropdown-menu shadow-invert" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('product','cana-de-acucar') }}">Cana de Açúcar</a>
                                <a class="dropdown-item" href="{{ route('product','grao') }}">Grãos</a>
                                <a class="dropdown-item" href="{{ route('product','etanol') }}">Etanol</a>
                                <a class="dropdown-item" href="{{ route('product','acucar') }}">Açúcar</a>
                                <a class="dropdown-item" href="{{ route('product','energia-eletrica') }}">Energia Elétrica</a>
                                <a class="dropdown-item" href="{{ route('product','certificacao') }}">Certificações</a>
                                </div>
                            </li>
                            <li class="nav-item  @if(Request::route()->getName() == 'client') active font-weight-bolder @endif">
                                <a class="nav-link" href="{{ route('client') }}">Clientes</a>
                            </li>
                            <li class="nav-item dropdown  @if(Request::route()->getName() == 'sustainability') active font-weight-bolder @endif">
                                <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Sustentabilidade
                                </a>
                                <div class="dropdown-menu shadow-invert" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('sustainability','socio-ambiental') }}">Sócio Ambiental</a>
                                <a class="dropdown-item" href="{{ route('sustainability','ambiental') }}">Ambiental</a>
                                </div>
                            </li>
                            <li class="nav-item @if(Request::route()->getName() == 'politic') active font-weight-bolder @endif">
                                <a class="nav-link" href="{{ route('politic') }}">Política</a>
                            </li>
                            <li class="nav-item @if(Request::route()->getName() == 'investor') active font-weight-bolder @endif">
                                <a class="nav-link" href="{{ route('investor') }}">Investidores</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="mask flex-center" style="background-image: url({{ url('assets/images/carrousel')}}/foto-1.jpg)">
                                <div class="container p-5 mt-5">
                                  <div class="row align-items-center mt-5">
                                    <div class="col-md-7 col-12 order-md-1 order-2 mt-5">
                                      <h4>Present your <br>
                                        awesome product</h4>
                                      <p>Lorem ipsum dolor sit amet. Reprehenderit, qui blanditiis quidem rerum <br>
                                        necessitatibus praesentium voluptatum deleniti atque corrupti.</p>
                                      <a href="#">BUY NOW</a> </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                          <div class="mask flex-center" style="background-image: url({{ url('assets/images/carrousel')}}/foto-2.jpg)">
                            <div class="container p-5 mt-5">
                              <div class="row align-items-center mt-5">
                                <div class="col-md-7 col-12 order-md-1 order-2 mt-5">
                                  <h4>Present your <br>
                                    awesome product</h4>
                                  <p>Lorem ipsum dolor sit amet. Reprehenderit, qui blanditiis quidem rerum <br>
                                    necessitatibus praesentium voluptatum deleniti atque corrupti.</p>
                                  <a href="#">BUY NOW</a> </div>
                               </div>
                            </div>
                          </div>
                        </div>
                        <div class="carousel-item">
                            <div class="mask flex-center" style="background-image: url({{ url('assets/images/carrousel')}}/foto-3.jpg)">
                                <div class="container p-5 mt-5">
                                <div class="row align-items-center mt-5">
                                  <div class="col-md-7 col-12 order-md-1 order-2 mt-5">
                                    <h4>Present your <br>
                                      awesome product</h4>
                                    <p>Lorem ipsum dolor sit amet. Reprehenderit, qui blanditiis quidem rerum <br>
                                      necessitatibus praesentium voluptatum deleniti atque corrupti.</p>
                                    <a href="#">BUY NOW</a> </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="mask flex-center" style="background-image: url({{ url('assets/images/carrousel')}}/foto-4.jpg)">
                                <div class="container p-5 mt-5">
                                <div class="row align-items-center mt-5">
                                  <div class="col-md-7 col-12 order-md-1 order-2 mt-5">
                                    <h4>Present your <br>
                                      awesome product</h4>
                                    <p>Lorem ipsum dolor sit amet. Reprehenderit, qui blanditiis quidem rerum <br>
                                      necessitatibus praesentium voluptatum deleniti atque corrupti.</p>
                                    <a href="#">BUY NOW</a> </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
                  </div>
            </header>
                @yield('content')
            </main>

            <footer class="footer mt-auto" style="border-top:2px rgb(48, 37, 150) solid">
                <div class="col-md-12 footer-background">
                    <div class="container p-5 ">
                        <div class="row">
                            <div class="col-md-6 text-white">© <?php echo date('Y'); ?> por Usina Sonora</div>
                            <div class="col-md-6 text-right">
                            <a href="{{ $facebook }}"><i class="fab fa-facebook" style="font-size:2rem;color:white"></i></a> &nbsp;
                            <a href="{{ $linkedin }}"><i class="fab fa-linkedin" style="font-size:2rem;color:white"></i></a>&nbsp;
                            <a href="{{ $instagram }}"><i class="fab fa-instagram" style="font-size:2rem;color:white"></i></a>&nbsp;
                            <a href="{{ $googleplus }}"><i class="fab fa-google-plus" style="font-size:2rem;color:white"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </body>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
        <script src="{{ url('assets/js/menu/main.js')}}"></script>
        <script src="{{ url('assets/js/carousel/main.js')}}"></script>
        @yield('script')
</html>
