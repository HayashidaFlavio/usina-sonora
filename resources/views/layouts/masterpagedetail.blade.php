<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <link type="text/css" rel="stylesheet" href="{{ url('assets/css/style.css') }}" />
        <link type="text/css" rel="stylesheet" href="{{ url('assets/css/menu.css') }}" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" integrity="sha512-RXf+QSDCUQs5uwRKaDoXt55jygZZm2V++WUZduaU/Ui/9EGp3f/2KZVahFZBKGH0s774sd3HmrhUy+SgOFQLVQ==" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        @yield('css')
    </head>
    <body>
        <header>
            <div class="col-md-12">
                <div class="row">
                    <a href="{{ route('login') }}" class="col-md-2 offset-md-9 btn btn-primary border-0" style="border-radius: 0 !important"><i class="fas fa-fw fa-user-tie"></i> Área do Investidor</a>
                </div>
            </div>
        <a href="{{ url('') }}"><img src="{{ url('vendor/adminlte/dist/img') }}/usinasonora-full.png" class="mx-auto d-block" title="Usina Sonora MS" alt="Usina Sonora MS">
        </a>
            <div class="container text-center">
                <nav>
                    <ul id="main">
                    <li>Usina
                    <ul class="drop">
                    <div>
                    <li><a href="/usina/historico" class="text-decoration-none text-white">Histórico</a></li>
                    <li><a href="/usina/grupo-economico" class="text-decoration-none text-white">Grupo Econômico</a></li>
                    </div>
                    </ul>
                    </li>
                    <li>Produtos
                    <ul class="drop">
                    <div>
                    <li><a href="/produto/cana-de-acucar" class="text-decoration-none text-white">Cana de acuçar</a></li>
                    <li><a href="/produto/graos" class="text-decoration-none text-white">Grãos</a></li>
                    <li><a href="/produto/etanol" class="text-decoration-none text-white">Etanol</a></li>
                    <li><a href="/produto/acucar" class="text-decoration-none text-white">Açucar</a></li>
                    <li><a href="/produto/energia-eletrica" class="text-decoration-none text-white">Energia Elétrica</a></li>
                    <li><a href="/produto/certificacoes" class="text-decoration-none text-white">Certificações</a></li>
                    </div>
                    </ul>
                    </li>
                    <li><a href="/cliente" class="text-decoration-none" style="color: black">Clientes</a></li>
                    <li>Sustentabilidade
                    <ul class="drop">
                        <div>
                          <li><a href="/sustentabilidade/socio-ambiental" class="text-decoration-none text-white">Sócio Ambiental</a></li>
                          <li><a href="/sustentabilidade/ambiental" class="text-decoration-none text-white">Ambiental</a></li>
                        </div>
                      </ul>
                    </li>
                    <li><a href="/politica" class="text-decoration-none" style="color: black">Política</a></li>
                    <li><a href="/investidor" class="text-decoration-none" style="color: black">Investidores</a></li>
                    <li><a href="/trabalhe" class="text-decoration-none" style="color: black">Trabalhe Conosco</a></li>
                    <li><a href="/contato" class="text-decoration-none"  style="color: black">Contato</a></li>
                      <div id="marker"></div>
                      </ul>
                    </nav>
            </div>
        </header>
        <main  role="main" class="flex-shrink-0">
            @yield('content')
        </main>

        <footer class="footer mt-auto " style="border-top:2px rgb(48, 37, 150) solid">
            <div class="col-md-12 sr-only" style="background-color: rgb(228, 228, 230)">
                <div class="container">
                    <div class="col-md-12 p-3">
                            <div class="col-md-4">
                                <h4>{{ $name }}</h4>
                                <p style="font-size: 0.95rem;color:rgb(104, 104, 104) !important">
                                    {{ $address }} <br>
                                    <b>Fone:</b> {{ $phone }} <br> <b>Fax:</b> {{ $fax }}
                                </p>
                            </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 footer-background">
                <div class="container p-5 ">
                    <div class="row">
                        <div class="col-md-6 text-white">© <?php echo date('Y'); ?> por Usina Sonora</div>
                        <div class="col-md-6 text-right">
                        <a href="{{ $facebook }}"><i class="fab fa-facebook" style="font-size:2rem;color:white"></i></a> &nbsp;
                        <a href="{{ $linkedin }}"><i class="fab fa-linkedin" style="font-size:2rem;color:white"></i></a>&nbsp;
                        <a href="{{ $instagram }}"><i class="fab fa-instagram" style="font-size:2rem;color:white"></i></a>&nbsp;
                        <a href="{{ $googleplus }}"><i class="fab fa-google-plus" style="font-size:2rem;color:white"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
</html>
